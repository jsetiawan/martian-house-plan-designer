/*
 * Tool.cpp
 *
 *  Created on: Mar 29, 2015
 *      Author: Jason
 */

#include "Tool.h"

const int BUTTON_DEPTH = 2;

Tool::Tool(std::string _name, bool _selected, bool _hover, Point _position, Point _size) :
	name(_name),
	selected(_selected),
	hover(_hover),
	position(_position),
	size(_size)
{}

void Tool::display(){

	Color4 buttonBG;
	Color4 leftBevel;
	Color4 topBevel;
	Color4 rightBevel;
	Color4 bottomBevel;
	Color4 textColor;

	if(hover || selected){
		if(hover){
			buttonBG = Color4(0.9, 0.9, 0.9, 1.0);
			leftBevel = Color4(1.0, 1.0, 1.0, 0.3);
			topBevel = Color4(1.0, 1.0, 1.0, 0.5);
			rightBevel = Color4(0.0, 0.0, 0.0, 0.1);
			bottomBevel = Color4(0.0, 0.0, 0.0, 0.2);
			textColor = Color4(0.0, 0.0, 0.0, 1.0);
		}
		if(selected){
			buttonBG = Color4(0.7, 0.7, 0.7, 1.0);
			leftBevel = Color4(0.0, 0.0, 0.0, 0.1);
			topBevel = Color4(0.0, 0.0, 0.0, 0.2);
			rightBevel = Color4(1.0, 1.0, 1.0, 0.1);
			bottomBevel = Color4(1.0, 1.0, 1.0, 0.2);
			textColor = Color4(1.0, 1.0, 1.0, 1.0);
		}
	}
	else{
		buttonBG = Color4(0.85, 0.85, 0.85, 1.0);
		leftBevel = Color4(1.0, 1.0, 1.0, 0.3);
		topBevel = Color4(1.0, 1.0, 1.0, 0.5);
		rightBevel = Color4(0.0, 0.0, 0.0, 0.1);
		bottomBevel = Color4(0.0, 0.0, 0.0, 0.2);
		textColor = Color4(0.0, 0.0, 0.0, 1.0);
	}


	//button background rectangle
	glColor4f(buttonBG.r, buttonBG.g, buttonBG.b, buttonBG.a);
	glBegin(GL_TRIANGLE_STRIP);
	  glVertex2f(position.x, position.y);
	  glVertex2f(position.x, position.y - size.y);
	  glVertex2f(position.x + size.x, position.y);
	  glVertex2f(position.x + size.x, position.y - size.y);
	glEnd();

	//left bevel
	glColor4f(leftBevel.r, leftBevel.g, leftBevel.b, leftBevel.a);
	glBegin(GL_TRIANGLE_STRIP);
	  glVertex2f(position.x, position.y);
	  glVertex2f(position.x, position.y - size.y);
	  glVertex2f(position.x + BUTTON_DEPTH, position.y - BUTTON_DEPTH);
	  glVertex2f(position.x + BUTTON_DEPTH, position.y - size.y + BUTTON_DEPTH);
	glEnd();

	//top bevel
	glColor4f(topBevel.r, topBevel.g, topBevel.b, topBevel.a);
	glBegin(GL_TRIANGLE_STRIP);
	  glVertex2f(position.x, position.y);
	  glVertex2f(position.x + size.x, position.y);
	  glVertex2f(position.x + BUTTON_DEPTH, position.y - BUTTON_DEPTH);
	  glVertex2f(position.x + size.x - BUTTON_DEPTH, position.y - BUTTON_DEPTH);
	glEnd();

	//right bevel
	glColor4f(rightBevel.r, rightBevel.g, rightBevel.b, rightBevel.a);
	glBegin(GL_TRIANGLE_STRIP);
	  glVertex2f(position.x + size.x, position.y);
	  glVertex2f(position.x + size.x, position.y - size.y);
	  glVertex2f(position.x + size.x - BUTTON_DEPTH, position.y - BUTTON_DEPTH);
	  glVertex2f(position.x + size.x - BUTTON_DEPTH, position.y - size.y + BUTTON_DEPTH);
	glEnd();

	//bottom bevel
	glColor4f(bottomBevel.r, bottomBevel.g, bottomBevel.b, bottomBevel.a);
	glBegin(GL_TRIANGLE_STRIP);
	  glVertex2f(position.x, position.y - size.y);
	  glVertex2f(position.x + size.x, position.y - size.y);
	  glVertex2f(position.x + BUTTON_DEPTH, position.y - size.y + BUTTON_DEPTH);
	  glVertex2f(position.x + size.x - BUTTON_DEPTH, position.y - size.y + BUTTON_DEPTH);
	glEnd();

	glColor4f(textColor.r, textColor.g, textColor.b, textColor.a);
	glRasterPos2f(position.x + 10, position.y - size.y/2 - 5);
	for(int i = 0; i < name.length(); i++){
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, name[i]);
	}
}


