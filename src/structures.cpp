/*
 * structures.cpp
 *
 *  Created on: Mar 29, 2015
 *      Author: Jason
 */
#include "structures.h"

const float DOOR_THICKNESS = 0.2;
const float WINDOW_THICKNESS = 0.15;


Point::Point(float _x, float _y) :
x(_x),
y(_y) {
}

Point::Point(){}

Color4::Color4(float _r, float _g, float _b, float _a) :
r(_r), g(_g), b(_b), a(_a){
}

Color4::Color4(){}


Wall::Wall(std::vector<Point> _wallPath) :
	wallPath(_wallPath){
}

Wall::Wall(){}


WallSegmenter::WallSegmenter(float _x, float _y, float _width, float _wallThickness, std::string _type, std::string _orientation) :
x(_x),
y(_y),
width(_width),
wallThickness(_wallThickness),
type(_type),
orientation(_orientation){
	if(type == "door"){
		thickness = DOOR_THICKNESS;
	}
	else{
		thickness = WINDOW_THICKNESS;
	}

	if(orientation == "top"){
		displayShiftX = 0;
		displayShiftY = wallThickness/2;
	}
	else if(orientation == "bottom"){
		displayShiftX = 0;
		displayShiftY = -wallThickness/2;
	}
	else if(orientation == "left"){
		displayShiftX = -wallThickness/2;
		displayShiftY = 0;
	}
	else if(orientation == "right"){
		displayShiftX = wallThickness/2;
		displayShiftY = 0;
	}
}

WallSegmenter::WallSegmenter(){
}

void WallSegmenter::save(std::ofstream &out){
	out << "OBJECT WallSegmenter" << std::endl;
	out << "X " << x << std::endl;
	out << "Y " << y << std::endl;
	out << "DISPLAY_SHIFT_X " << displayShiftX << std::endl;
	out << "DISPLAY_SHIFT_Y " << displayShiftY << std::endl;
	out << "WIDTH " << width << std::endl;
	out << "THICKNESS " << thickness << std::endl;
	out << "WALL_THICKNESS " << wallThickness << std::endl;
	out << "TYPE " << type << std::endl;
	out << "ORIENTATION " << orientation << std::endl;

}

void WallSegmenter::load(std::ifstream &in, std::string object){
	std::string line;

	if(object != "WallSegmenter"){
		std::cerr << "Expected WallSegmenter, got " << object << std::endl;
		exit(1);
	}

	getline(in, line);
	if(line.substr(0, 2) != "X "){
		std::cerr << "Expected X, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(2, std::string::npos)) >> x;

	getline(in, line);
	if(line.substr(0, 2) != "Y "){
		std::cerr << "Expected Y, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(2, std::string::npos)) >> y;

	getline(in, line);
	if(line.substr(0, 16) != "DISPLAY_SHIFT_X "){
		std::cerr << "Expected DISPLAY_SHIFT_X, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(16, std::string::npos)) >> displayShiftX;

	getline(in, line);
	if(line.substr(0, 16) != "DISPLAY_SHIFT_Y "){
		std::cerr << "Expected DISPLAY_SHIFT_Y, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(16, std::string::npos)) >> displayShiftY;

	getline(in, line);
	if(line.substr(0, 6) != "WIDTH "){
		std::cerr << "Expected WIDTH, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(6, std::string::npos)) >> width;

	getline(in, line);
	if(line.substr(0, 10) != "THICKNESS "){
		std::cerr << "Expected THICKNESS, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(10, std::string::npos)) >> thickness;

	getline(in, line);
	if(line.substr(0, 15) != "WALL_THICKNESS "){
		std::cerr << "Expected WALL_THICKNESS, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(15, std::string::npos)) >> wallThickness;

	getline(in, line);
	if(line.substr(0, 5) != "TYPE "){
		std::cerr << "Expected TYPE, got " << line << std::endl;
		exit(1);
	}
	type = line.substr(5, std::string::npos);

	getline(in, line);
	if(line.substr(0, 12) != "ORIENTATION "){
		std::cerr << "Expected ORIENTATION, got " << line << std::endl;
		exit(1);
	}
	orientation = line.substr(12, std::string::npos);
}

void WallSegmenter::display(){
	if(orientation == "top" || orientation == "bottom"){
		if(type == "door"){
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_TRIANGLE_STRIP); //draw red door
			  glVertex2f(x - width/2 + displayShiftX, y + thickness/2 + displayShiftY);
			  glVertex2f(x + width/2 + displayShiftX, y + thickness/2 + displayShiftY);
			  glVertex2f(x - width/2 + displayShiftX, y - thickness/2 + displayShiftY);
			  glVertex2f(x + width/2 + displayShiftX, y - thickness/2 + displayShiftY);
			glEnd();
		}

		glColor3f(0.0, 0.0, 0.0);
		glBegin(GL_LINES); //draw segmenter outline
		  glVertex2f(x - width/2 + displayShiftX, y + thickness/2 + displayShiftY);
		  glVertex2f(x + width/2 + displayShiftX, y + thickness/2 + displayShiftY);
		  glVertex2f(x - width/2 + displayShiftX, y - thickness/2 + displayShiftY);
		  glVertex2f(x + width/2 + displayShiftX, y - thickness/2 + displayShiftY);
		glEnd();
	}
	else if(orientation == "left" || orientation == "right"){
		if(type == "door"){
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_TRIANGLE_STRIP); //draw red door
			  glVertex2f(x - thickness/2 + displayShiftX, y + width/2 + displayShiftY);
			  glVertex2f(x - thickness/2 + displayShiftX, y - width/2 + displayShiftY);
			  glVertex2f(x + thickness/2 + displayShiftX, y + width/2 + displayShiftY);
			  glVertex2f(x + thickness/2 + displayShiftX, y - width/2 + displayShiftY);
			glEnd();
		}

		glColor3f(0.0, 0.0, 0.0);
		glBegin(GL_LINES); //draw segmenter outline
		  glVertex2f(x - thickness/2 + displayShiftX, y + width/2 + displayShiftY);
		  glVertex2f(x - thickness/2 + displayShiftX, y - width/2 + displayShiftY);
		  glVertex2f(x + thickness/2 + displayShiftX, y + width/2 + displayShiftY);
		  glVertex2f(x + thickness/2 + displayShiftX, y - width/2 + displayShiftY);
		glEnd();
	}
}

