/*
 * Furnishing.h
 *
 *  Created on: Mar 30, 2015
 *      Author: Jason
 */

#ifndef FURNISHING_H_
#define FURNISHING_H_

#include <math.h>
#include <string>
#include <vector>
#include <GL/glut.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include "structures.h"
#include "FurnishingDimensions.h"

class Furnishing {
public:
	std::string name;
	Point position;
	float width;
	float height;
	float rotation;
	float animatedRotation;
	std::vector< std::vector<Point> > vertices;

	Furnishing(std::string _name, Point _position);
	Furnishing(std::string _name, std::vector< std::vector<Point> > _vertices, float _rotation, Point _position, float _width, float _height);
	Furnishing();
	void setSleeperVertices();
	void setShowerVertices();
	void setRobeVertices();
	void setSinkVertices();
	void setWCVertices();
	void setTableVertices();
	void setChairVertices();
	void save(std::ofstream &out);
	void load(std::ifstream &in, std::string object);
	void display();
};



#endif /* FURNISHING_H_ */
