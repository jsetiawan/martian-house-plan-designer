/*
 * Tool.h
 *
 *  Created on: Mar 29, 2015
 *      Author: Jason
 */

#ifndef TOOL_H_
#define TOOL_H_

#include <GL/glut.h>
#include "structures.h"

class Tool {
public:
	std::string name;
	bool selected;
	bool hover;
	Point position, size;

	Tool(std::string _name, bool _selected, bool _hover, Point _position, Point _size);

	void display();
};

#endif /* TOOL_H_ */
