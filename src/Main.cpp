//#include <GL/glut.h>
#include <iostream>

#include "Display.h"
//#include <list>
//#include <vector>
//#include <math.h>
#include "structures.h"
#include "Module.h"
#include "Globals.h"
#include "Toolbar.h"

Globals global;
std::vector<Module> modules;
Toolbar tools(global, modules);

void init(void)   /* initialization function  */
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable( GL_LINE_SMOOTH );
	glEnable( GL_POLYGON_SMOOTH );
	glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
	glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );

	glClearColor(1.0, 1.0, 1.0, 0.0); /* set background color to white */
}

void displayCB(void){
	global.projectScreen();
	glClear( GL_COLOR_BUFFER_BIT); /* clear the screen window */

	//Draw world elements
	global.projectWorld();

	displayGrid(global);
	displayModules(modules);

	if(global.showMagneticCursor){
		displayMagneticCursor(global);
	}

	if(global.showMagneticRectangle){
		displayMagneticRectangle(global);
	}

	if(global.showPressureDoorPreview){
		displayPressureDoorPreview(global, modules);
	}

	if(global.showWindowPreview){
		displayWindowPreview(global, modules);
	}

	if(global.showFurnishingPreview){
		displayFurnishingPreview(global);
	}

	//Draw panel elements
	global.projectPanel();

	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_LINES);
	  glVertex2f(global.screenSize.x - global.panelWidth, global.screenSize.y);
	  glVertex2f(global.screenSize.x - global.panelWidth, 0);
	glEnd();

	tools.display();

	glFlush(); /* Complete any pending operations */
	glutSwapBuffers();

	//update animations
	global.animatedMousePoint.x += (global.magneticMousePoint.x - global.animatedMousePoint.x)/2.5;
	global.animatedMousePoint.y += (global.magneticMousePoint.y - global.animatedMousePoint.y)/2.5;
	global.animatedWorldSize += (global.worldSize - global.animatedWorldSize)/4;
}

void keyCB(unsigned char key, int x, int y) /* keyboard callback function,
                                               called on key press */
{
//	std::cout << key << std::endl << std::flush;
	if (key == 'q')
		exit(0);

//	if (key == '')
//		std::cout << "ctrl" << std::endl << std::flush;;

	if(key == ' '){
		global.worldDragMode = true;
	}

	tools.keyHandler(key, x, y);
}

void keyUpCB(unsigned char key, int x, int y){
	if(key == ' '){
		global.worldDragMode = false;
	}
}

//void specialCB(int key, int x, int y){
//	if(key == 114){
//		global.ctrlHeld = true;
//	}
//}
//
//void specialUpCB(int key, int x, int y){
//	if(key == 114){
//		global.ctrlHeld = false;
//	}
//}

void mouseCB(int button, int state, int x, int y){
	tools.mouseHandler(button, state, x, y);

}

void passiveMotionCB(int x, int y){
	tools.passiveMotionHandler(x, y);
}

void motionCB(int x, int y){
	tools.motionHandler(x, y);
}

void timer(int value){
	glutPostRedisplay();
	glutTimerFunc(global.refreshSpeed, timer, 0);
}

void reshape(int width, int height){
	global.screenSize = Point(width, height);
	global.worldRatio = (float)(width - global.panelWidth)/(float)height;
	glViewport(0, 0, width, height);
	glutPostRedisplay();
}

int main(int argc, char *argv[]) {

	global.saveState(modules);

	//initialize global variables
	global.initialScreenSize = Point(1024, 768);
	global.screenSize = Point(1024, 768);
	global.gridSize = 30.0;
	global.worldFocus = Point(0, 0);
	global.animatedWorldSize = 9.0;
	global.worldSize = global.animatedWorldSize;
	global.worldRatio = 1.0;
	global.panelWidth = 256.0;
	global.showMagneticCursor = false;
	global.refreshSpeed = 20;
	global.animatedMousePoint = Point(0.0, -global.animatedWorldSize);
	global.worldDragMode = false;

	//toolbar setup
	tools.addTool("High Grade Module");
	tools.addTool("Low Grade Module");
	tools.addTool("Pressure Seal Door");
	tools.addTool("Window");
//	tools.addTool("Internal Walls");
//	tools.addTool("Door");
	tools.addTool("Move Furnishing");
	tools.addTool("Rotate Furnishing");
	tools.addTool("Remove Furnishing");
	tools.addTool("Sleeper");
	tools.addTool("Shower");
	tools.addTool("Robe");
	tools.addTool("Sink");
	tools.addTool("WC");
	tools.addTool("Table");
	tools.addTool("Chair");
	tools.selectTool("High Grade Module");

	glutInit(&argc, argv); /* initialize GLUT system */
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(1024, 768); /* width=400pixels height=500pixels */
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Martian House"); /* create screen window */
	glutKeyboardFunc(keyCB); /* register keyboard callback function*/
	glutKeyboardUpFunc(keyUpCB);
//	glutSpecialFunc(specialCB);
//	glutSpecialUpFunc(specialUpCB);
	glutMouseFunc(mouseCB);
	glutPassiveMotionFunc(passiveMotionCB);
	glutMotionFunc(motionCB);
	glutDisplayFunc(displayCB); /* register display callback function*/
	glutReshapeFunc(reshape);
	glutTimerFunc(0, timer, 0);
	init();      /* call init */

	glutMainLoop(); /* show screen window, call display and
					   start processing events... */
	/* execution never reaches this point */

	return 0;
}
