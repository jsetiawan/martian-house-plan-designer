/*
 * Furnishing.cpp
 *
 *  Created on: Mar 30, 2015
 *      Author: Jason
 */

#include "Furnishing.h"

Furnishing::Furnishing(std::string _name, Point _position) : name(_name), position(_position){
	if(name == "Sleeper"){
		setSleeperVertices();
		width = SLEEPER_WIDTH;
		height = SLEEPER_HEIGHT;
	}
	else if(name == "Shower"){
		setShowerVertices();
		width = SHOWER_WIDTH;
		height = SHOWER_HEIGHT;
	}
	else if(name == "Robe"){
		setRobeVertices();
		width = ROBE_WIDTH;
		height = ROBE_HEIGHT;
	}
	else if(name == "Sink"){
		setSinkVertices();
		width = SINK_WIDTH;
		height = SINK_HEIGHT;
	}
	else if(name == "WC"){
		setWCVertices();
		width = WC_WIDTH;
		height = WC_HEIGHT;
	}
	else if(name == "Table"){
		setTableVertices();
		width = TABLE_WIDTH;
		height = TABLE_HEIGHT;
	}
	else if(name == "Chair"){
		setChairVertices();
		width = CHAIR_WIDTH;
		height = CHAIR_HEIGHT;
	}

	rotation = 0.0;
	animatedRotation = 0.0;
}

Furnishing::Furnishing(std::string _name, std::vector< std::vector<Point> > _vertices, float _rotation, Point _position, float _width, float _height) :
	name(_name), vertices(_vertices), rotation(_rotation), position(_position), width(_width), height(_height)
	{

	}

Furnishing::Furnishing(){}

void Furnishing::setSleeperVertices(){
	std::vector<Point> bed;
	std::vector<Point> pillow;

//	bed.push_back(Point(-0.5, -1.0));
//	bed.push_back(Point(-0.5, 1.0));
//	bed.push_back(Point(0.5, 1.0));
//	bed.push_back(Point(0.5, -1.0));

	float bedX = 0.4;
	float bedY = 0.9;
	float angleAdjust = 0;

	for(float i = 0; i < M_PI/2; i += (M_PI/2)/24){
		bed.push_back(Point(bedX + 0.1*cos(i + angleAdjust), bedY + 0.1*sin(i + angleAdjust)));
	}

	bedX = -0.4;
	bedY = 0.9;
	angleAdjust = M_PI/2;

	for(float i = 0; i < M_PI/2; i += (M_PI/2)/24){
		bed.push_back(Point(bedX + 0.1*cos(i + angleAdjust), bedY + 0.1*sin(i + angleAdjust)));
	}

	bedX = -0.4;
	bedY = -0.9;
	angleAdjust = M_PI;

	for(float i = 0; i < M_PI/2; i += (M_PI/2)/24){
		bed.push_back(Point(bedX + 0.1*cos(i + angleAdjust), bedY + 0.1*sin(i + angleAdjust)));
	}

	bedX = 0.4;
	bedY = -0.9;
	angleAdjust = 3*M_PI/2;

	for(float i = 0; i < M_PI/2; i += (M_PI/2)/24){
		bed.push_back(Point(bedX + 0.1*cos(i + angleAdjust), bedY + 0.1*sin(i + angleAdjust)));
	}

	float pillowX = 0.3;
	float pillowY = 0.8;
	angleAdjust = 0;

	for(float i = 0; i < M_PI/2; i += (M_PI/2)/24){
		pillow.push_back(Point(pillowX + 0.125*cos(i + angleAdjust), pillowY + 0.125*sin(i + angleAdjust)));
	}

	pillowX = -0.3;
	pillowY = 0.8;
	angleAdjust = M_PI/2;

	for(float i = 0; i < M_PI/2; i += (M_PI/2)/24){
		pillow.push_back(Point(pillowX + 0.125*cos(i + angleAdjust), pillowY + 0.125*sin(i + angleAdjust)));
	}

	pillowX = -0.3;
	pillowY = 0.6;
	angleAdjust = M_PI;

	for(float i = 0; i < M_PI/2; i += (M_PI/2)/24){
		pillow.push_back(Point(pillowX + 0.125*cos(i + angleAdjust), pillowY + 0.125*sin(i + angleAdjust)));
	}

	pillowX = 0.3;
	pillowY = 0.6;
	angleAdjust = 3*M_PI/2;

	for(float i = 0; i < M_PI/2; i += (M_PI/2)/24){
		pillow.push_back(Point(pillowX + 0.125*cos(i + angleAdjust), pillowY + 0.125*sin(i + angleAdjust)));
	}

	vertices.push_back(bed);
	vertices.push_back(pillow);
}

void Furnishing::setShowerVertices(){
	std::vector<Point> cubicle;
	std::vector<Point> cubicleInner;
	std::vector<Point> showerHead;
	std::vector<Point> showerArm;

	cubicle.push_back(Point(-0.75, -0.75));
	cubicle.push_back(Point(-0.75, 0.75));
	cubicle.push_back(Point(0.75, 0.75));
	cubicle.push_back(Point(0.75, -0.75));

	cubicleInner.push_back(Point(-0.7, -0.7));
	cubicleInner.push_back(Point(-0.7, 0.7));
	cubicleInner.push_back(Point(0.7, 0.7));
	cubicleInner.push_back(Point(0.7, -0.7));

	for(float i = 0; i < 2 * M_PI; i += (2*M_PI)/12){
		showerHead.push_back(Point(0.125*cos(i), 0.1 + 0.125*sin(i)));
	}

	showerArm.push_back(Point(-0.025, 0.2));
	showerArm.push_back(Point(-0.025, 0.75));
	showerArm.push_back(Point(0.025, 0.75));
	showerArm.push_back(Point(0.025, 0.2));



	vertices.push_back(cubicle);
	vertices.push_back(cubicleInner);
	vertices.push_back(showerHead);
	vertices.push_back(showerArm);
}

void Furnishing::setRobeVertices(){
	std::vector<Point> wardrobe;

	wardrobe.push_back(Point(-0.25, -1.0));
	wardrobe.push_back(Point(-0.25, 1.0));
	wardrobe.push_back(Point(0.25, 1.0));
	wardrobe.push_back(Point(0.25, -1.0));

	vertices.push_back(wardrobe);
}

void Furnishing::setSinkVertices(){
	std::vector<Point> sinkTop;
	std::vector<Point> sink;
	std::vector<Point> tap;

	sinkTop.push_back(Point(-0.5, -0.25));
	sinkTop.push_back(Point(-0.5, 0.25));
	sinkTop.push_back(Point(0.5, 0.25));
	sinkTop.push_back(Point(0.5, -0.25));

	for(float i = 0; i < 2 * M_PI; i += (2*M_PI)/24){
		sink.push_back(Point(0.3*cos(i), 0.2*sin(i)));
	}

	tap.push_back(Point(-0.06, 0));
	tap.push_back(Point(-0.06, 0.25));
	tap.push_back(Point(0.06, 0.25));
	tap.push_back(Point(0.06, 0));

	vertices.push_back(sinkTop);
	vertices.push_back(sink);
	vertices.push_back(tap);
}

void Furnishing::setWCVertices(){
	std::vector<Point> tank;
	std::vector<Point> button;
	std::vector<Point> outerRing;
	std::vector<Point> innerRing;

	tank.push_back(Point(-0.4, 0.25));
	tank.push_back(Point(-0.4, 0.5));
	tank.push_back(Point(0.4, 0.5));
	tank.push_back(Point(0.4, 0.25));

	for(float i = 0; i < 2*M_PI; i += (2*M_PI)/12){
		button.push_back(Point(0.05*cos(i), 0.375 + 0.05*sin(i)));
	}

	for(float i = 0; i < 2*M_PI; i += (2*M_PI)/24){
		outerRing.push_back(Point(0.3*cos(i), -0.125 + 0.375*sin(i)));
	}

	for(float i = 0; i < 2*M_PI; i += (2*M_PI)/24){
		innerRing.push_back(Point(0.175*cos(i), -0.125 + 0.25*sin(i)));
	}

	vertices.push_back(tank);
	vertices.push_back(button);
	vertices.push_back(outerRing);
	vertices.push_back(innerRing);
}

void Furnishing::setTableVertices(){
	std::vector<Point> tableTop;

	tableTop.push_back(Point(-0.75, -0.5));
	tableTop.push_back(Point(-0.75, 0.5));
	tableTop.push_back(Point(0.75, 0.5));
	tableTop.push_back(Point(0.75, -0.5));

	vertices.push_back(tableTop);
}

void Furnishing::setChairVertices(){
	std::vector<Point> seat;
	std::vector<Point> chairBack;

	seat.push_back(Point(-0.25, -0.25));
	seat.push_back(Point(-0.25, 0.25));
	seat.push_back(Point(0.25, 0.25));
	seat.push_back(Point(0.25, -0.25));

	chairBack.push_back(Point(-0.25, 0.15));
	chairBack.push_back(Point(-0.25, 0.25));
	chairBack.push_back(Point(0.25, 0.25));
	chairBack.push_back(Point(0.25, 0.15));

	vertices.push_back(seat);
	vertices.push_back(chairBack);
}

void Furnishing::save(std::ofstream &out){
	out << "OBJECT Furnishing" << std::endl;
	out << "NAME " << name << std::endl;
	out << "POSITION " << position.x << " " << position.y << std::endl;
	out << "WIDTH " << width << std::endl;
	out << "HEIGHT " << height << std::endl;
	out << "ROTATION " << rotation << std::endl;
	out << "ANIMATED_ROTATION " << animatedRotation << std::endl;
}

void Furnishing::load(std::ifstream &in, std::string object){
	std::string line;

	if(object != "Furnishing"){
		std::cerr << "Expected Furnishing, got " << object << std::endl;
		exit(1);
	}

	getline(in, line);
	if(line.substr(0, 5) != "NAME "){
		std::cerr << "Expected NAME, got " << line << std::endl;
		exit(1);
	}
	name = line.substr(5, std::string::npos);

	getline(in, line);
	if(line.substr(0, 9) != "POSITION "){
		std::cerr << "Expected POSITION, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(9, std::string::npos)) >> position.x >> position.y;

	getline(in, line);
	if(line.substr(0, 6) != "WIDTH "){
		std::cerr << "Expected WIDTH, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(6, std::string::npos)) >> width;

	getline(in, line);
	if(line.substr(0, 7) != "HEIGHT "){
		std::cerr << "Expected HEIGHT, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(7, std::string::npos)) >> height;

	getline(in, line);
	if(line.substr(0, 9) != "ROTATION "){
		std::cerr << "Expected ROTATION, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(9, std::string::npos)) >> rotation;

	getline(in, line);
	if(line.substr(0, 18) != "ANIMATED_ROTATION "){
		std::cerr << "Expected ANIMATED_ROTATION, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(18, std::string::npos)) >> animatedRotation;

	if(name == "Sleeper"){
		setSleeperVertices();
	}
	else if(name == "Shower"){
		setShowerVertices();
	}
	else if(name == "Robe"){
		setRobeVertices();
	}
	else if(name == "Sink"){
		setSinkVertices();
	}
	else if(name == "WC"){
		setWCVertices();
	}
	else if(name == "Table"){
		setTableVertices();
	}
	else if(name == "Chair"){
		setChairVertices();
	}
}

void Furnishing::display(){
	glColor3f(0.0, 0.0, 0.0);
	glLineWidth(0.5);
	glPushMatrix();
	glTranslatef(position.x, position.y, 0);
	glRotatef(animatedRotation, 0, 0, 1);
	for(int i = 0; i < vertices.size(); i++){
		glBegin(GL_LINE_LOOP);
		  for(int j = 0; j < vertices[i].size(); j++){
			  glVertex2f(vertices[i][j].x, vertices[i][j].y);
		  }
		glEnd();
	}
	glPopMatrix();

	glPushMatrix();
	glTranslatef(position.x, position.y, 0);
	glColor4f(0.0, 0.0, 0.0, 1.0);
	glRasterPos2f(-width/2, height/2 + 0.1);
	for(int i = 0; i < name.length(); i++){
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, name[i]);
	}
	glPopMatrix();

	//update rotation animation
	animatedRotation += (rotation - animatedRotation)/4;
	if(animatedRotation <= 0){
		animatedRotation += 360;
		rotation += 360;
	}
}
