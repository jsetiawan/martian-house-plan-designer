/*
 * Globals.h
 *
 *  Created on: Mar 29, 2015
 *      Author: Jason
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <GL/glut.h>
#include <math.h>
#include <fstream>
#include <sstream>
#include "structures.h"
#include "Furnishing.h"
#include "Module.h"

class Globals{
public:
	// The screen coordinates in pixels, x and y corresponding with width and height
	Point screenSize;

	// Initial screen coordinates, used to adjust toolbar placement, in case of window resizing
	Point initialScreenSize;

	// Center of the world window, in world coordinates
	Point worldFocus;

	// Effectively zoom amount. World window shows worldSize*2 units vertically
	float worldSize;

	// World size while animating
	float animatedWorldSize;

	// Aspect ratio of world window
	float worldRatio;

	// Width of panel on the right, in pixels
	float panelWidth;

	// Mouse coordinates that stick to grid
	Point magneticMousePoint;

	// Mouse coordinates, animated when traveling between grid coordinates
	Point animatedMousePoint;

	// Magnetic cursor is drawn when true
	bool showMagneticCursor;

	// Magnetic rectangle (module drawing preview) is drawn when true
	bool showMagneticRectangle;

	// Tracks whether a module that is being drawn can be created
	bool validRectangle;

	// Stores a point that is clicked. Used by tools that require dragging
	Point clickedPoint;

	// Screen refresh speed in ms
	float refreshSpeed;

	// World can be dragged around when true. Activated by holding spacebar
	bool worldDragMode;

	// Tracks whether a pressure door can be legally created
	bool pressureDoorPlaceable;

	// Pressure door preview is drawn when true
	bool showPressureDoorPreview;

	// Window preview is drawn when true
	bool showWindowPreview;

	// Tracks whether a window can be legally created
	bool windowPlaceable;

	// Tracks whether a furnishing can be legally created/placed
	bool furnishingPlaceable;

	// Dimensions used to draw furnishing previews
	float furnishingPreviewWidth;
	float furnishingPreviewHeight;

	// Temporarily held furnishing, taken from a module. Used while moving a furnishing
	Furnishing movingFurnishing;

	// Records whether a furnishing is being moved
	bool furnishingDragMode;

	// Furnishing preview is drawn when true
	bool showFurnishingPreview;

	// When a furnishing is being moved, the index of the module is kept here, in case it needs to return to that module (e.g. invalid placement)
	int originalModuleIndexOfFurnishing;

	// When true, magnetized cursor shifts by a quarter unit, along X or Y axis. Used when placing objects whose size is not a whole unit (e.g. Windows, some furnishings)
	bool cursorQuarterShiftX;
	bool cursorQuarterShiftY;

	// Index of module that might have an action done upon it (add door/window/furnishing, etc.)
	int activeModuleIndex;

	// Records whether module is being drawn
	bool moduleDrawingMode;

	// Dimension of grid/canvas, horizontally and vertically
	float gridSize;

	// Amount of undo's performed
	int undoCounter;

	// Keeps track of drawing history (undo and redo)
	std::vector< std::vector<Module> > savedStates;

	// Sets up projection matrices
	void projectScreen();
	void projectWorld();
	void projectPanel();

	// Returns mouse position in the world
	Point mouseToWorld(int mouseX, int mouseY);

	// Returns mouse position in the world, magnetized to whole units if 'nearestHalf' is false, half unit increments otherwise
	void magnetizeMouse(int mouseX, int mouseY, bool nearestHalf);

	// Returns whether the mouse is in the world
	bool mouseInWorld(int x, int y);

	// Returns whether the mouse is in the grid/canvas, in the world
	bool mouseInWorldBoundary(int x, int y);

	// Saves program state to savedStates vector. Called each time an action is done.
	void saveState(std::vector<Module> state);

	// Returns a previous or follow program state. Called when undo or redo is invoked
	std::vector<Module> retrieveState(std::string action);

	// Saves passed program state to 'text.dat'
	void writeToFile(std::vector<Module> modules);

	// Loads program state from 'text.dat'
	void readFromFile(std::vector<Module>& modules);
};

#endif /* GLOBALS_H_ */
