/*
 * display.h
 *
 *  Created on: Mar 29, 2015
 *      Author: Jason
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <GL/glut.h>
#include "structures.h"
#include <math.h>
#include <vector>
#include "Module.h"
#include "Globals.h"

void displayGrid(Globals& global);

void displayModules(std::vector<Module>& modules);

void displayMagneticCursor(Globals& global);

void displayMagneticRectangle(Globals& global);

void displayPressureDoorPreview(Globals& global, std::vector<Module>& modules);

void displayWindowPreview(Globals& global, std::vector<Module>& modules);

void displayFurnishingPreview(Globals& global);

#endif /* DISPLAY_H_ */
