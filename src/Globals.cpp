/*
 * Globals.cpp
 *
 *  Created on: Mar 29, 2015
 *      Author: Jason
 */

#include "Globals.h"

const int MAX_STATES = 10; // Effectively amount of undo's that can be stored

void Globals::projectScreen(){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, screenSize.x, 0, screenSize.y);
	glViewport(0, 0, screenSize.x, screenSize.y);
}

void Globals::projectWorld(){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluOrtho2D(worldFocus.x - worldRatio*animatedWorldSize,
			   worldFocus.x + worldRatio*animatedWorldSize,
			   worldFocus.y - animatedWorldSize,
			   worldFocus.y + animatedWorldSize);
	glViewport(0, 0, screenSize.x - panelWidth, screenSize.y);
}

void Globals::projectPanel(){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(screenSize.x - panelWidth, screenSize.x, 0, screenSize.y);
	glViewport(screenSize.x - panelWidth, 0, panelWidth, screenSize.y);
}

Point Globals::mouseToWorld(int mouseX, int mouseY){
	float worldX = ((2 * mouseX)/(screenSize.x - panelWidth) - 1) * animatedWorldSize * worldRatio + worldFocus.x;
	float worldY = ((1 - (mouseY)/(screenSize.y)) * 2 - 1) * animatedWorldSize + worldFocus.y;

	return Point(worldX, worldY);
}

void Globals::magnetizeMouse(int mouseX, int mouseY, bool nearestHalf){

	Point worldPoint = mouseToWorld(mouseX, mouseY);
	float shiftX = 0;
	float shiftY = 0;

	if(cursorQuarterShiftX){
		shiftX = 0.25;
	}
	if(cursorQuarterShiftY){
		shiftY = 0.25;
	}

	float roundedX = round(worldPoint.x - shiftX) + shiftX;
	float roundedY = round(worldPoint.y - shiftY) + shiftY;

	if(nearestHalf == true){
		float remainderX = worldPoint.x - shiftX - trunc(worldPoint.x - shiftX);
		float remainderY = worldPoint.y - shiftY - trunc(worldPoint.y - shiftY);

		if(remainderX >= 0.25 && remainderX < 0.5){
			roundedX += 0.5;
		}
		else if(remainderX >= 0.5 && remainderX < 0.75){
			roundedX -= 0.5;
		}
		else if(remainderX <= -0.25 && remainderX > -0.5){
			roundedX -= 0.5;
		}
		else if(remainderX <= -0.5 && remainderX > -0.75){
			roundedX += 0.5;
		}

		if(remainderY >= 0.25 && remainderY < 0.5){
			roundedY += 0.5;
		}
		else if(remainderY >= 0.5 && remainderY < 0.75){
			roundedY -= 0.5;
		}
		else if(remainderY <= -0.25 && remainderY > -0.5){
			roundedY -= 0.5;
		}
		else if(remainderY <= -0.5 && remainderY > -0.75){
			roundedY += 0.5;
		}
	}
	else if(showMagneticRectangle){
		if(fabs(fmod(clickedPoint.x, 1)) == 0.5){
			roundedX = round(worldPoint.x - 0.5) + 0.5;
		}
		if(fabs(fmod(clickedPoint.y, 1)) == 0.5){
			roundedY = round(worldPoint.y - 0.5) + 0.5;
		}
	}

	magneticMousePoint = Point(roundedX, roundedY);
}

bool Globals::mouseInWorld(int x, int y){
	bool inWorld;
	if(x < screenSize.x - panelWidth){
		inWorld = true;
	}
	else{
		inWorld = false;
	}

	return inWorld;
}

bool Globals::mouseInWorldBoundary(int x, int y){
	bool inWorldBoundary;
	Point worldMousePoint = mouseToWorld(x, y);

	if(fabs(worldMousePoint.x) <= gridSize/2 &&
	   fabs(worldMousePoint.y) <= gridSize/2){
		inWorldBoundary = true;
	}
	else{
		inWorldBoundary = false;
	}

	return inWorldBoundary;
}

void Globals::saveState(std::vector<Module> state){
	if(undoCounter > 0){
		for(int i = 0; i < undoCounter; i++){
			savedStates.erase(savedStates.end());
		}
		undoCounter = 0;
	}

	if(savedStates.size() >= MAX_STATES){
		savedStates.erase(savedStates.begin());
	}

	savedStates.push_back(state);
}

std::vector<Module> Globals::retrieveState(std::string action){
	if(action == "undo"){
		if(undoCounter + 1 < savedStates.size()){
			undoCounter++;
		}
	}
	else if(action == "redo"){
		if(undoCounter > 0){
			undoCounter--;
		}
	}

	return savedStates[savedStates.size() - 1 - undoCounter];
}

void Globals::writeToFile(std::vector<Module> modules){
	std::ofstream out("text.dat");
	out << "MODULES_SIZE " << modules.size() << std::endl;
	for(int i = 0; i < modules.size(); i++){
		modules[i].save(out);
	}
	std::cout << "Data saved to file" << std::endl << std::flush;
}

void Globals::readFromFile(std::vector<Module>& modules){



	modules.clear();

	std::ifstream in("text.dat");
	std::string line;
	int vectorSize;

	getline(in, line);
	if(line.substr(0, 13) != "MODULES_SIZE "){
		std::cerr << "Expected MODULES_SIZE, got " << line << std::endl;
		exit (1);
	}
	std::stringstream(line.substr(13, std::string::npos)) >> vectorSize;

	for(int i = 0; i < vectorSize; i++){
		getline(in, line);
		if (line.substr(0, 7) != "OBJECT ") {
			std::cerr << "Expected OBJECT, got " << line << std::endl;
			exit (1);
		}
		modules.push_back(Module());
		modules[i].load(in, line.substr(7, std::string::npos));
	}
	glutPostRedisplay();
	std::cout << "Data loaded from file" << std::endl << std::flush;
}




