#include "Module.h"

const int BOTTOM = 0;
const int LEFT = 1;
const int TOP = 2;
const int RIGHT = 3;
const int CORNER_SMOOTHNESS = 20; // amount of lines that make up rounded corner of high grade modules
const float CORNER_RADIUS = 1.0; //Internal radius of corners in high grade modules
const float DOOR_THICKNESS = 0.2;
const float WINDOW_THICKNESS = 0.15;

Module::Module(float _width, float _height, bool _highGrade, Point _position) :
width(_width),
height(_height),
highGrade(_highGrade),
position(_position)	{
	if(highGrade == false){
		windowWidth = 0.5;
		wallThickness = 0.5;
	}
	else{
		windowWidth = 1.5;
		wallThickness = 0.25;
	}
	doorWidth = 1.0;

	divideWalls();
	generateDrawableWallVertices();
}

Module::Module(
		Point _position,
		float _width,
		float _height,
		bool _highGrade,
		std::vector<WallSegmenter> _segmenters,
		std::vector<Wall> _walls,
		std::vector<Furnishing> _furnishings,
		float _doorWidth,
		float _windowWidth,
		float _wallThickness,
		std::vector< std::vector<Point> > _wallLineLoopVertices,
		std::vector< std::vector<Point> > _wallTriangleStripVertices
		) :
		position(_position),
		width(_width),
		height(_height),
		highGrade(_highGrade),
		segmenters(_segmenters),
		walls(_walls),
		furnishings(_furnishings),
		doorWidth(_doorWidth),
		windowWidth(_windowWidth),
		wallThickness(_wallThickness),
		wallLineLoopVertices(_wallLineLoopVertices),
		wallTriangleStripVertices(_wallTriangleStripVertices)
		{
		}

Module::Module(){}

void Module::addWindow(Point windowPosition){

	if(windowPosition.y > position.y && windowPosition.y < position.y + height){
		if(windowPosition.x == position.x){
			segmenters.push_back(WallSegmenter(windowPosition.x, windowPosition.y, windowWidth, wallThickness, "window", "left"));
		}
		else if(windowPosition.x == position.x + width){
			segmenters.push_back(WallSegmenter(windowPosition.x, windowPosition.y, windowWidth, wallThickness, "window", "right"));
		}
	}
	else if(windowPosition.x > position.x && windowPosition.x < position.x + width){
		if(windowPosition.y == position.y){
			segmenters.push_back(WallSegmenter(windowPosition.x, windowPosition.y, windowWidth, wallThickness, "window", "bottom"));
		}
		else if(windowPosition.y == position.y + height){
			segmenters.push_back(WallSegmenter(windowPosition.x, windowPosition.y, windowWidth, wallThickness, "window", "top"));
		}
	}
	divideWalls();
	generateDrawableWallVertices();
}

void Module::addPressureDoor(Point doorPosition){
	if(doorPosition.y > position.y && doorPosition.y < position.y + height){
		if(doorPosition.x == position.x){
			segmenters.push_back(WallSegmenter(doorPosition.x, doorPosition.y, doorWidth, wallThickness, "door", "left"));
		}
		else if(doorPosition.x == position.x + width){
			segmenters.push_back(WallSegmenter(doorPosition.x, doorPosition.y, doorWidth, wallThickness, "door", "right"));
		}
	}
	else if(doorPosition.x > position.x && doorPosition.x < position.x + width){
		if(doorPosition.y == position.y){
			segmenters.push_back(WallSegmenter(doorPosition.x, doorPosition.y, doorWidth, wallThickness, "door", "bottom"));
		}
		else if(doorPosition.y == position.y + height){
			segmenters.push_back(WallSegmenter(doorPosition.x, doorPosition.y, doorWidth, wallThickness, "door", "top"));
		}
	}

	divideWalls();
	generateDrawableWallVertices();
}

void Module::addFurnishing(std::string type, Point position){
	furnishings.push_back(Furnishing(type, position));
}

void Module::divideWalls(){

	walls.clear();
	if(segmenters.size() > 0){
		std::vector< std::vector<WallSegmenter> > segmentedWalls(4);

		//populate nested std::vector with segmenters, grouped by wall
		for(int i = 0; i < segmenters.size(); i++){
			if(segmenters[i].y == position.y){
				segmentedWalls[BOTTOM].push_back(segmenters[i]);
			}
			else if(segmenters[i].x == position.x){
				segmentedWalls[LEFT].push_back(segmenters[i]);
			}
			else if(segmenters[i].y == position.y + height){
				segmentedWalls[TOP].push_back(segmenters[i]);
			}
			else if(segmenters[i].x == position.x + width){
				segmentedWalls[RIGHT].push_back(segmenters[i]);
			}
		}

		//Insertion sort for four walls in a CW manner, starting from bottom right
		int j;
		WallSegmenter temp;

		//Sort BOTTOM segmentedWalls std::vector
		for(int i = 1; i < segmentedWalls[BOTTOM].size(); i++){
			j = i;

			while (j > 0 && segmentedWalls[BOTTOM][j].x > segmentedWalls[BOTTOM][j - 1].x){
				temp = segmentedWalls[BOTTOM][j];
				segmentedWalls[BOTTOM][j] = segmentedWalls[BOTTOM][j - 1];
				segmentedWalls[BOTTOM][j - 1] = temp;
				j--;
			}
		}

		//Sort LEFT segmentedWalls std::vector
		for(int i = 1; i < segmentedWalls[LEFT].size(); i++){
			j = i;

			while (j > 0 && segmentedWalls[LEFT][j].y < segmentedWalls[LEFT][j - 1].y){
				temp = segmentedWalls[LEFT][j];
				segmentedWalls[LEFT][j] = segmentedWalls[LEFT][j - 1];
				segmentedWalls[LEFT][j - 1] = temp;
				j--;
			}
		}

		//Sort TOP segmentedWalls std::vector
		for(int i = 1; i < segmentedWalls[TOP].size(); i++){
			j = i;

			while (j > 0 && segmentedWalls[TOP][j].x < segmentedWalls[TOP][j - 1].x){
				temp = segmentedWalls[TOP][j];
				segmentedWalls[TOP][j] = segmentedWalls[TOP][j - 1];
				segmentedWalls[TOP][j - 1] = temp;
				j--;
			}
		}

		//Sort RIGHT segmentedWalls std::vector
		for(int i = 1; i < segmentedWalls[RIGHT].size(); i++){
			j = i;

			while (j > 0 && segmentedWalls[RIGHT][j].y > segmentedWalls[RIGHT][j - 1].y){
				temp = segmentedWalls[RIGHT][j];
				segmentedWalls[RIGHT][j] = segmentedWalls[RIGHT][j - 1];
				segmentedWalls[RIGHT][j - 1] = temp;
				j--;
			}
		}

		int currentWall = -1;

		// Find wall to start from. Wall must contain a segmenter.

		if(segmentedWalls[BOTTOM].size() > 0){
			currentWall = BOTTOM;
		}
		else if(segmentedWalls[LEFT].size() > 0){
			currentWall = LEFT;
		}
		else if(segmentedWalls[TOP].size() > 0){
			currentWall = TOP;
		}
		else if(segmentedWalls[RIGHT].size() > 0){
			currentWall = RIGHT;
		}

		int initialWall = currentWall;
		WallSegmenter initialSegmenter = segmentedWalls[initialWall].front();

		int preInitialWall = initialWall - 1;

		if(preInitialWall == -1){
			preInitialWall = 3;
		}

		Point final;
		int x1SpacerCoefficient;
		int y1SpacerCoefficient;
		int x2SpacerCoefficient;
		int y2SpacerCoefficient;

		for(int i = 0; i < segmenters.size(); i++){
			Wall wallSegment;
			int segmentersEncountered = 0;

			while(segmentersEncountered < 2){

				if(i == segmenters.size() - 1 && currentWall == preInitialWall){
					segmentedWalls[initialWall].push_back(initialSegmenter);
				}

				if(currentWall == BOTTOM){
					final = Point(position.x, position.y);
					x1SpacerCoefficient = -1;
					y1SpacerCoefficient = 0;
					x2SpacerCoefficient = 1;
					y2SpacerCoefficient = 0;
				}
				else if(currentWall == LEFT){
					final = Point(position.x, position.y + height);
					x1SpacerCoefficient = 0;
					y1SpacerCoefficient = 1;
					x2SpacerCoefficient = 0;
					y2SpacerCoefficient = -1;
				}
				else if(currentWall == TOP){
					final = Point(position.x + width, position.y + height);
					x1SpacerCoefficient = 1;
					y1SpacerCoefficient = 0;
					x2SpacerCoefficient = -1;
					y2SpacerCoefficient = 0;
				}
				else if(currentWall == RIGHT){
					final = Point(position.x + width, position.y);
					x1SpacerCoefficient = 0;
					y1SpacerCoefficient = -1;
					x2SpacerCoefficient = 0;
					y2SpacerCoefficient = 1;
				}

				if(segmentedWalls[currentWall].size() == 0){
					wallSegment.wallPath.push_back(final);
					currentWall++;
					if(currentWall == 4){
						currentWall = 0;
					}
				}
				else{
					float spacer;
					if(segmentedWalls[currentWall].front().type == "door"){
						spacer = doorWidth/2;
					}
					else if(segmentedWalls[currentWall].front().type == "window"){
						spacer = windowWidth/2;
					}

					if(segmentersEncountered == 0){
						wallSegment.wallPath.push_back(Point(segmentedWalls[currentWall].front().x + x1SpacerCoefficient * spacer, segmentedWalls[currentWall].front().y + y1SpacerCoefficient * spacer));
						segmentedWalls[currentWall].erase(segmentedWalls[currentWall].begin());
					}
					else if(segmentersEncountered == 1){
						wallSegment.wallPath.push_back(Point(segmentedWalls[currentWall].front().x + x2SpacerCoefficient * spacer, segmentedWalls[currentWall].front().y + y2SpacerCoefficient * spacer));
					}
					segmentersEncountered++;
				}
			}
			walls.push_back(wallSegment);
		}
	}
}

void Module::generateDrawableWallVertices(){
	wallLineLoopVertices.clear();
	wallTriangleStripVertices.clear();

	if(walls.size() > 0){
		if(!highGrade){
			for(int i = 0; i < walls.size(); i++){
				std::vector<Point> wallShape;

				for(int j = 0; j < walls[i].wallPath.size(); j++){
					wallShape.push_back(walls[i].wallPath[j]);
				}

				for(int j = walls[i].wallPath.size() - 1; j > -1; j--){

					float adjustedX, adjustedY;

					if(walls[i].wallPath[j].x == position.x){
						adjustedX = position.x - wallThickness;
					}
					else if(walls[i].wallPath[j].x == position.x + width){
						adjustedX = position.x + width + wallThickness;
					}
					else{
						adjustedX = walls[i].wallPath[j].x;
					}

					if(walls[i].wallPath[j].y == position.y){
						adjustedY = position.y - wallThickness;
					}
					else if(walls[i].wallPath[j].y == position.y + height){
						adjustedY = position.y + height + wallThickness;
					}
					else{
						adjustedY = walls[i].wallPath[j].y;
					}

					wallShape.push_back(Point(adjustedX, adjustedY));
				}

				wallLineLoopVertices.push_back(wallShape);
			}
		}

		else if(highGrade){
			for(int i = 0; i < walls.size(); i++){
				std::vector<Point> wallShape;

				for(int j = 0; j < walls[i].wallPath.size(); j++){

					float angleAdjust = -1;
					float xOffsetCoefficient, yOffsetCoefficient;

					if(walls[i].wallPath[j].x == position.x && walls[i].wallPath[j].y == position.y){ // bottom left corner
						angleAdjust = M_PI;
						xOffsetCoefficient = 1;
						yOffsetCoefficient = 1;
					}
					else if(walls[i].wallPath[j].x == position.x && walls[i].wallPath[j].y == position.y + height){ // top left corner
						angleAdjust = M_PI/2;
						xOffsetCoefficient = 1;
						yOffsetCoefficient = -1;
					}
					else if(walls[i].wallPath[j].x == position.x + width && walls[i].wallPath[j].y == position.y + height){ // top right corner
						angleAdjust = 0;
						xOffsetCoefficient = -1;
						yOffsetCoefficient = -1;
					}
					else if(walls[i].wallPath[j].x == position.x + width && walls[i].wallPath[j].y == position.y){ // bottom right corner
						angleAdjust = (3*M_PI)/2;
						xOffsetCoefficient = -1;
						yOffsetCoefficient = 1;
					}

					if(angleAdjust == -1){ // current point is not a corner
						wallShape.push_back(walls[i].wallPath[j]);
					}
					else{ // current point is a corner, create quarter circle in CW manner
						for(float k = M_PI/2; k > 0; k -= (M_PI/2)/CORNER_SMOOTHNESS){
							wallShape.push_back(Point(
										walls[i].wallPath[j].x + xOffsetCoefficient*CORNER_RADIUS + CORNER_RADIUS*cos(k + angleAdjust),
										walls[i].wallPath[j].y + yOffsetCoefficient*CORNER_RADIUS + CORNER_RADIUS*sin(k + angleAdjust))
									);
						}
					}
				}
				for(int j = walls[i].wallPath.size() - 1; j > -1; j--){
					float angleAdjust = -1;
					float xOffsetCoefficient, yOffsetCoefficient, adjustedX, adjustedY;

					if(walls[i].wallPath[j].x == position.x && walls[i].wallPath[j].y == position.y){ // bottom left corner
						angleAdjust = M_PI;
						xOffsetCoefficient = 1;
						yOffsetCoefficient = 1;
					}
					else if(walls[i].wallPath[j].x == position.x && walls[i].wallPath[j].y == position.y + height){ // top left corner
						angleAdjust = M_PI/2;
						xOffsetCoefficient = 1;
						yOffsetCoefficient = -1;
					}
					else if(walls[i].wallPath[j].x == position.x + width && walls[i].wallPath[j].y == position.y + height){ // top right corner
						angleAdjust = 0;
						xOffsetCoefficient = -1;
						yOffsetCoefficient = -1;
					}
					else if(walls[i].wallPath[j].x == position.x + width && walls[i].wallPath[j].y == position.y){ // bottom right corner
						angleAdjust = (3*M_PI)/2;
						xOffsetCoefficient = -1;
						yOffsetCoefficient = 1;
					}

					if(angleAdjust == -1){ // current point is not a corner
						if(walls[i].wallPath[j].x == position.x){
							adjustedX = position.x - wallThickness;
						}
						else if(walls[i].wallPath[j].x == position.x + width){
							adjustedX = position.x + width + wallThickness;
						}
						else{
							adjustedX = walls[i].wallPath[j].x;
						}

						if(walls[i].wallPath[j].y == position.y){
							adjustedY = position.y - wallThickness;
						}
						else if(walls[i].wallPath[j].y == position.y + height){
							adjustedY = position.y + height + wallThickness;
						}
						else{
							adjustedY = walls[i].wallPath[j].y;
						}

						wallShape.push_back(Point(adjustedX, adjustedY));
					}
					else{ // current point is a corner, create quarter circle in CCW manner
						for(float k = 0; k < M_PI/2; k += (M_PI/2)/CORNER_SMOOTHNESS){
							wallShape.push_back(Point(
										walls[i].wallPath[j].x + xOffsetCoefficient*CORNER_RADIUS + (CORNER_RADIUS + wallThickness)*cos(k + angleAdjust),
										walls[i].wallPath[j].y + yOffsetCoefficient*CORNER_RADIUS + (CORNER_RADIUS + wallThickness)*sin(k + angleAdjust))
									);
						}
					}
				}
				wallLineLoopVertices.push_back(wallShape);
			}
		}

		//create triangle strip vertices from newly generated line loop vertices

		for(int i = 0; i < wallLineLoopVertices.size(); i++){
			std::vector<Point> wallShape;
			for(int j = 0; j < wallLineLoopVertices[i].size()/2; j++){
				wallShape.push_back(wallLineLoopVertices[i][j]);
				wallShape.push_back(wallLineLoopVertices[i][wallLineLoopVertices[i].size() - j - 1]);
			}
			wallTriangleStripVertices.push_back(wallShape);
		}
	}
	else if(walls.size() == 0){
		if(!highGrade){
			std::vector<Point> wallShape;
			wallShape.push_back(Point(position.x, position.y));
			wallShape.push_back(Point(position.x, position.y + height));
			wallShape.push_back(Point(position.x + width, position.y + height));
			wallShape.push_back(Point(position.x + width, position.y));
			wallLineLoopVertices.push_back(wallShape);

			wallShape.clear();

			wallShape.push_back(Point(position.x - wallThickness, position.y - wallThickness));
			wallShape.push_back(Point(position.x - wallThickness, position.y + height + wallThickness));
			wallShape.push_back(Point(position.x + width + wallThickness, position.y + height + wallThickness));
			wallShape.push_back(Point(position.x + width + wallThickness, position.y - wallThickness));
			wallLineLoopVertices.push_back(wallShape);
		}
		else if(highGrade){
			std::vector<Point> wallShape1;
			std::vector<Point> wallShape2;

			for(float i = M_PI/2; i > 0; i -= (M_PI/2)/CORNER_SMOOTHNESS){
				wallShape1.push_back(Point(
					position.x + CORNER_RADIUS + CORNER_RADIUS*cos(i + M_PI),
					position.y + CORNER_RADIUS + CORNER_RADIUS*sin(i + M_PI)
				));

				wallShape2.push_back(Point(
					position.x + CORNER_RADIUS + (CORNER_RADIUS + wallThickness)*cos(i + M_PI),
					position.y + CORNER_RADIUS + (CORNER_RADIUS + wallThickness)*sin(i + M_PI)
				));
			}

			for(float i = M_PI/2; i > 0; i -= (M_PI/2)/CORNER_SMOOTHNESS){
				wallShape1.push_back(Point(
					position.x + CORNER_RADIUS + CORNER_RADIUS*cos(i + M_PI/2),
					position.y + height - CORNER_RADIUS + CORNER_RADIUS*sin(i + M_PI/2)
				));

				wallShape2.push_back(Point(
					position.x + CORNER_RADIUS + (CORNER_RADIUS + wallThickness)*cos(i + M_PI/2),
					position.y + height - CORNER_RADIUS + (CORNER_RADIUS + wallThickness)*sin(i + M_PI/2)
				));
			}

			for(float i = M_PI/2; i > 0; i -= (M_PI/2)/CORNER_SMOOTHNESS){
				wallShape1.push_back(Point(
					position.x + width - CORNER_RADIUS + CORNER_RADIUS*cos(i),
					position.y + height - CORNER_RADIUS + CORNER_RADIUS*sin(i)
				));

				wallShape2.push_back(Point(
					position.x + width - CORNER_RADIUS + (CORNER_RADIUS + wallThickness)*cos(i),
					position.y + height - CORNER_RADIUS + (CORNER_RADIUS + wallThickness)*sin(i)
				));
			}

			for(float i = M_PI/2; i > 0; i -= (M_PI/2)/CORNER_SMOOTHNESS){
				wallShape1.push_back(Point(
					position.x + width - CORNER_RADIUS + CORNER_RADIUS*cos(i + (3*M_PI)/2),
					position.y + CORNER_RADIUS + CORNER_RADIUS*sin(i + (3*M_PI)/2)
				));

				wallShape2.push_back(Point(
					position.x + width - CORNER_RADIUS + (CORNER_RADIUS + wallThickness)*cos(i + (3*M_PI)/2),
					position.y + CORNER_RADIUS + (CORNER_RADIUS + wallThickness)*sin(i + (3*M_PI)/2)
				));
			}

			wallLineLoopVertices.push_back(wallShape1);
			wallLineLoopVertices.push_back(wallShape2);
		}

		//create triangle strip vertices from newly generated line loop vertices
		std::vector<Point> wallShape;
		for(int i = 0; i < wallLineLoopVertices[0].size(); i++){
			wallShape.push_back(wallLineLoopVertices[0][i]);
			wallShape.push_back(wallLineLoopVertices[1][i]);
		}

		wallShape.push_back(wallLineLoopVertices[0][0]);
		wallShape.push_back(wallLineLoopVertices[1][0]);

		wallTriangleStripVertices.push_back(wallShape);
	}
}

bool Module::verifyNonIntersecting(Point pos, float w, float h, bool hGrade){
	bool nonIntersecting;
	float buffer1;
	float buffer2;
	if(highGrade == true){
		buffer1 = 0.25;
	}
	else{
		buffer1 = 0.5;
	}
	if(hGrade == true){
		buffer2 = 0.25;
	}
	else{
		buffer2 = 0.5;
	}

	std::vector<Point> corners1;
	std::vector<Point> corners2;

	corners1.push_back(Point(position.x - buffer1, position.y - buffer1));
	corners1.push_back(Point(position.x - buffer1, position.y + height + buffer1));
	corners1.push_back(Point(position.x + width + buffer1, position.y + height + buffer1));
	corners1.push_back(Point(position.x + width + buffer1, position.y - buffer1));

	corners2.push_back(Point(pos.x - buffer2, pos.y - buffer2));
	corners2.push_back(Point(pos.x - buffer2, pos.y + h + buffer2));
	corners2.push_back(Point(pos.x + w + buffer2, pos.y + h + buffer2));
	corners2.push_back(Point(pos.x + w + buffer2, pos.y - buffer2));

	int intersectionCounter = 0;

	for(int i = 0; i < corners1.size(); i++){
		if(corners1[i].x >= pos.x && corners1[i].x <= pos.x + w && corners1[i].y >= pos.y && corners1[i].y <= pos.y + h){
			intersectionCounter++;
		}
	}

	for(int i = 0; i < corners2.size(); i++){
		if(corners2[i].x >= position.x && corners2[i].x <= position.x + width && corners2[i].y >= position.y && corners2[i].y <= position.y + height){
			intersectionCounter++;
		}
	}

	if(position.x <= pos.x && position.x + width >= pos.x + w && position.y >= pos.y && position.y + height <= pos.y + h){
		intersectionCounter++;
	}
	if(pos.x <= position.x && pos.x + w >= position.x + width && pos.y >= position.y && pos.y + h <= position.y + height){
		intersectionCounter++;
	}

	if(pos.x == position.x && pos.x + w == position.x + width){
		if(pos.y >= position.y && pos.y <= position.y + height){
			intersectionCounter++;
		}
		else if(pos.y + h >= position.y && pos.y + h <= position.y + height){
			intersectionCounter++;
		}
	}

	if(pos.y == position.y && pos.y + h == position.y + height){
		if(pos.x >= position.x && pos.x <= position.x + width){
			intersectionCounter++;
		}
		else if(pos.x + w >= position.x && pos.x + w <= position.x + width){
			intersectionCounter++;
		}
	}

	if(intersectionCounter > 0){
		nonIntersecting = false;
	}
	else{
		nonIntersecting = true;
	}

	return nonIntersecting;
}

bool Module::onWall(float x, float y){

	float buffer = 0.1;
	bool onWall = false;

	if(x == position.x || x == position.x + width){
		if(y >= position.y + buffer && y <= position.y + height - buffer){
			onWall = true;
		}
	}
	else if(y == position.y || y == position.y + height){
		if(x >= position.x + buffer && x <= position.x + width - buffer){
			onWall = true;
		}
	}

	return onWall;
}

bool Module::verifySegmenterProximity(float x, float y, std::string type){
	float minimumProximity;
	float cornerProximity;
	float proximityAdjustment1;
	bool valid = true;

	if(highGrade == true){
		minimumProximity = 1.0;
		cornerProximity = CORNER_RADIUS + 0.5;
	}
	else{
		minimumProximity = 1.5;
		cornerProximity = 0.5;
	}

	if(type == "window"){
		proximityAdjustment1 = windowWidth/2;
	}
	else{
		proximityAdjustment1 = doorWidth/2;
	}

	if(x == position.x || x == position.x + width){
		for(int i = 0; i < segmenters.size(); i++){
			if(segmenters[i].x == x){
				float proximityAdjustment2;
				if(segmenters[i].type == "window"){
					proximityAdjustment2 = windowWidth/2;
				}
				else{
					proximityAdjustment2 = doorWidth/2;
				}

				if(fabs(segmenters[i].y - y) < minimumProximity + proximityAdjustment1 + proximityAdjustment2){
					valid = false;
				}
			}
		}
		if(fabs(position.y - y) < cornerProximity + proximityAdjustment1 || fabs(position.y + height - y) < cornerProximity + proximityAdjustment1 ){
			valid = false;
		}
	}
	else if(y == position.y || y == position.y + height){
		for(int i = 0; i < segmenters.size(); i++){
			if(segmenters[i].y == y){
				float proximityAdjustment2;
				if(segmenters[i].type == "window"){
					proximityAdjustment2 = windowWidth/2;
				}
				else{
					proximityAdjustment2 = doorWidth/2;
				}

				if(fabs(segmenters[i].x - x) < minimumProximity + proximityAdjustment1 + proximityAdjustment2){
					valid = false;
				}
			}
		}
		if(fabs(position.x - x) < cornerProximity + proximityAdjustment1  || fabs(position.x + width - x) < cornerProximity + proximityAdjustment1 ){
			valid = false;
		}
	}

	return valid;
}

bool Module::verifyLegalFurnishingPlacement(float w, float h, Point pos, int intersectionExceptionIndex){
	bool valid = true;

	//check if furnishing is out of module boundary
	if(pos.x + w/2 > position.x + width ||
	   pos.x - w/2 < position.x ||
	   pos.y + h/2 > position.y + height ||
	   pos.y - h/2 < position.y){
		valid = false;
	}

	if(highGrade == true){
		if(pos.x + w/2 == position.x + width || pos.x - w/2 == position.x){
			if(pos.y + h/2 > position.y + height - CORNER_RADIUS || pos.y - h/2 < position.y + CORNER_RADIUS){
				valid = false;
			}
		}
		else if(pos.y + h/2 == position.y + height || pos.y - h/2 == position.y){
			if(pos.x + w/2 > position.x + width - CORNER_RADIUS || pos.x - w/2 < position.x + CORNER_RADIUS){
				valid = false;
			}
		}
	}

	//check if furnishing intersects with any existing furnishings
	for(int i = 0; i < furnishings.size(); i++){
		if(i != intersectionExceptionIndex){
			if(fabs(pos.x - furnishings[i].position.x) < w/2 + furnishings[i].width/2 &&
			   fabs(pos.y - furnishings[i].position.y) < h/2 + furnishings[i].height/2){
				valid = false;
			}
		}
	}

	return valid;
}

int Module::getClickedFurnishingIndex(Point clicked){
	int furnishingIndex = -1;

	for(int i = 0; i < furnishings.size(); i++){
		if(fabs(clicked.x - furnishings[i].position.x) < furnishings[i].width/2 &&
		   fabs(clicked.y - furnishings[i].position.y) < furnishings[i].height/2){
			furnishingIndex = i;
		}
	}

	return furnishingIndex;
}

Furnishing Module::retrieveFurnishing(int furnishingIndex){
	Furnishing retrieved;
	retrieved = furnishings[furnishingIndex];
	furnishings.erase(furnishings.begin() + furnishingIndex);

	return retrieved;
}

void Module::restoreFurnishing(Furnishing returnedFurnishing){
	furnishings.push_back(returnedFurnishing);
}

void Module::rotateFurnishing(int furnishingIndex){
	float rotatedWidth = furnishings[furnishingIndex].height;
	float rotatedHeight = furnishings[furnishingIndex].width;
	Point position = furnishings[furnishingIndex].position;

	if(verifyLegalFurnishingPlacement(rotatedWidth, rotatedHeight, position, furnishingIndex)){
		furnishings[furnishingIndex].rotation -= 90.0;
		furnishings[furnishingIndex].width = rotatedWidth;
		furnishings[furnishingIndex].height = rotatedHeight;
	}
	else{
		furnishings[furnishingIndex].rotation -= 180.0;
	}
}

void Module::removeFurnishing(int furnishingIndex){
	furnishings.erase(furnishings.begin() + furnishingIndex);
}

void Module::save(std::ofstream &out){
	out << "OBJECT Module" << std::endl;
	out << "POSITION " << position.x << " " << position.y << std::endl;
	out << "WIDTH " << width << std::endl;
	out << "HEIGHT " << height << std::endl;
	out << "HIGH_GRADE " << highGrade << std::endl;
	out << "DOOR_WIDTH " << doorWidth << std::endl;
	out << "WINDOW_WIDTH " << windowWidth << std::endl;
	out << "WALL_THICKNESS " << wallThickness << std::endl;
	out << "SEGMENTERS_SIZE " << segmenters.size() << std::endl;

	for(int i = 0; i < segmenters.size(); i++){
		segmenters[i].save(out);
	}

	out << "FURNISHINGS_SIZE " << furnishings.size() << std::endl;
	for(int i = 0; i < furnishings.size(); i++){
		furnishings[i].save(out);
	}
}

void Module::load(std::ifstream &in, std::string object){
	std::string line;

	if(object != "Module"){
		std::cerr << "Expected Module, got " << object << std::endl;
		exit(1);
	}

	getline(in, line);
	if(line.substr(0, 9) != "POSITION "){
		std::cerr << "Expected POSITION, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(9, std::string::npos)) >> position.x >> position.y;

	getline(in, line);
	if(line.substr(0, 6) != "WIDTH "){
		std::cerr << "Expected WIDTH, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(6, std::string::npos)) >> width;

	getline(in, line);
	if(line.substr(0, 7) != "HEIGHT "){
		std::cerr << "Expected HEIGHT, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(7, std::string::npos)) >> height;

	getline(in, line);
	if(line.substr(0, 11) != "HIGH_GRADE "){
		std::cerr << "Expected HIGH_GRADE, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(11, std::string::npos)) >> highGrade;

	getline(in, line);
	if(line.substr(0, 11) != "DOOR_WIDTH "){
		std::cerr << "Expected DOOR_WIDTH, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(11, std::string::npos)) >> doorWidth;

	getline(in, line);
	if(line.substr(0, 13) != "WINDOW_WIDTH "){
		std::cerr << "Expected HEIGHT, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(13, std::string::npos)) >> windowWidth;

	getline(in, line);
	if(line.substr(0, 15) != "WALL_THICKNESS "){
		std::cerr << "Expected WALL_THICKNESS, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(15, std::string::npos)) >> wallThickness;

	int vectorSize;

	getline(in, line);
	if(line.substr(0, 16) != "SEGMENTERS_SIZE "){
		std::cerr << "Expected SEGMENTERS_SIZE, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(16, std::string::npos)) >> vectorSize;

	for(int i = 0; i < vectorSize; i++){
		getline(in, line);
		if (line.substr(0, 7) != "OBJECT ") {
			std::cerr << "Expected OBJECT, got " << line << std::endl;
			exit (1);
		}
		segmenters.push_back(WallSegmenter());
		segmenters[i].load(in, line.substr(7, std::string::npos));
	}

	divideWalls();
	generateDrawableWallVertices();

	getline(in, line);
	if(line.substr(0, 17) != "FURNISHINGS_SIZE "){
		std::cerr << "Expected FURNISHINGS_SIZE, got " << line << std::endl;
		exit(1);
	}
	std::stringstream(line.substr(17, std::string::npos)) >> vectorSize;

	for(int i = 0; i < vectorSize; i++){
		getline(in, line);
		if (line.substr(0, 7) != "OBJECT ") {
			std::cerr << "Expected OBJECT, got " << line << std::endl;
			exit (1);
		}
		furnishings.push_back(Furnishing());
		furnishings[i].load(in, line.substr(7, std::string::npos));
	}

}

void Module::display(){
	//display doors and windows
	for(int i = 0; i < segmenters.size(); i++){
		segmenters[i].display();
	}

	//display furnishings
	for(int i = 0; i < furnishings.size(); i++){
		furnishings[i].display();
	}

	//display wall fill
	glColor4f(1.0, 0.0, 0.0, 0.5);
	for(int i = 0; i < wallTriangleStripVertices.size(); i++){
		glBegin(GL_TRIANGLE_STRIP);
			for(int j = 0; j < wallTriangleStripVertices[i].size(); j++){
				glVertex2f(wallTriangleStripVertices[i][j].x, wallTriangleStripVertices[i][j].y);
			}
		glEnd();
	}

	//display wall outline
	glLineWidth(1);
	glColor3f(0.0, 0.0, 0.0);
	for(int i = 0; i < wallLineLoopVertices.size(); i++){
		glBegin(GL_LINE_LOOP);
			for(int j = 0; j < wallLineLoopVertices[i].size(); j++){
				glVertex2f(wallLineLoopVertices[i][j].x, wallLineLoopVertices[i][j].y);
			}
		glEnd();
	}
}

