/*
 * display.cpp
 *
 *  Created on: Mar 29, 2015
 *      Author: Jason
 */
#include "Display.h"

//const int global.gridSize = 30;
const float MAGNETIC_CURSOR_RADIUS = 0.10;

void displayGrid(Globals& global){
	glLineWidth(0.5);
	glBegin(GL_LINES);
	for(int i = -global.gridSize/2; i < global.gridSize/2; i++){
		glColor4f(0.0, 0.0, 0.0, 0.2);
		glVertex2f(-global.gridSize/2, i);
		glVertex2f(global.gridSize/2, i);

		glColor4f(0.0, 0.0, 0.0, 0.05);
		glVertex2f(-global.gridSize/2, i + 0.5);
		glVertex2f(global.gridSize/2, i + 0.5);
	}
	for(int i = -global.gridSize/2; i < global.gridSize/2; i++){
		glColor4f(0.0, 0.0, 0.0, 0.2);
		glVertex2f(i, -global.gridSize/2);
		glVertex2f(i, global.gridSize/2);

		glColor4f(0.0, 0.0, 0.0, 0.05);
		glVertex2f(i + 0.5, -global.gridSize/2);
		glVertex2f(i + 0.5, global.gridSize/2);
	}

	glColor4f(0.0, 0.0, 0.0, 0.2);
	glVertex2f(-global.gridSize/2, global.gridSize/2);
	glVertex2f(global.gridSize/2, global.gridSize/2);
	glVertex2f(global.gridSize/2, -global.gridSize/2);
	glVertex2f(global.gridSize/2, global.gridSize/2);
	glEnd();
}

void displayModules(std::vector<Module>& modules){
	for(int i = 0; i < modules.size(); i++){
		modules[i].display();
	}
}

void displayMagneticCursor(Globals& global){
	glColor4f(0.0, 0.65, 0.8, 0.6);
	glBegin(GL_TRIANGLE_FAN);
	  glVertex2f(global.animatedMousePoint.x, global.animatedMousePoint.y);

	  for(float i = 0; i <= 2*M_PI; i += (2*M_PI)/12){
		  glVertex2f(global.animatedMousePoint.x + MAGNETIC_CURSOR_RADIUS * cos(i), global.animatedMousePoint.y + MAGNETIC_CURSOR_RADIUS * sin(i));
	  }
	glEnd();
}

void displayMagneticRectangle(Globals& global){

	Color4 rectangleFill;
	Color4 rectangleOutline;

	if(global.validRectangle){
		rectangleFill = Color4(0.0, 1.0, 0.0, 0.2);
		rectangleOutline = Color4(0.0, 1.0, 0.0, 0.8);
	}
	else{
		rectangleFill = Color4(1.0, 0.0, 0.0, 0.2);
		rectangleOutline = Color4(1.0, 0.0, 0.0, 0.8);
	}

	glColor4f(rectangleFill.r, rectangleFill.g, rectangleFill.b, rectangleFill.a);
	glBegin(GL_TRIANGLE_STRIP);
	  glVertex2f(global.clickedPoint.x, global.clickedPoint.y);
	  glVertex2f(global.clickedPoint.x, global.animatedMousePoint.y);
	  glVertex2f(global.animatedMousePoint.x, global.clickedPoint.y);
	  glVertex2f(global.animatedMousePoint.x, global.animatedMousePoint.y);
	glEnd();

	glColor4f(rectangleOutline.r, rectangleOutline.g, rectangleOutline.b, rectangleOutline.a);
	glBegin(GL_LINE_LOOP);
	  glVertex2f(global.clickedPoint.x, global.clickedPoint.y);
	  glVertex2f(global.clickedPoint.x, global.animatedMousePoint.y);
	  glVertex2f(global.animatedMousePoint.x, global.animatedMousePoint.y);
	  glVertex2f(global.animatedMousePoint.x, global.clickedPoint.y);
	glEnd();
}

void displayPressureDoorPreview(Globals& global, std::vector<Module>& modules){

	std::string orientation;
	float shift;
	float thickness = modules[global.activeModuleIndex].wallThickness;
	Color4 doorColor;

	//left wall
	if(global.magneticMousePoint.x == modules[global.activeModuleIndex].position.x){
		orientation = "vertical";
		shift = -thickness/2;
	}//right wall
	else if(global.magneticMousePoint.x == modules[global.activeModuleIndex].position.x + modules[global.activeModuleIndex].width){
		orientation = "vertical";
		shift = thickness/2;

	}//bottom wall
	else if(global.magneticMousePoint.y == modules[global.activeModuleIndex].position.y){
		orientation = "horizontal";
		shift = -thickness/2;
	}//top wall
	else if(global.magneticMousePoint.y == modules[global.activeModuleIndex].position.y + modules[global.activeModuleIndex].height){
		orientation = "horizontal";
		shift = thickness/2;
	}

	if(global.pressureDoorPlaceable){
		doorColor = Color4(0.0, 1.0, 0.0, 0.6);
	}
	else{
		doorColor = Color4(1.0, 0.0, 0.0, 0.6);
	}

	if(orientation == "vertical"){
		glPushMatrix();
		glTranslatef(global.magneticMousePoint.x + shift, global.magneticMousePoint.y, 0);
		glColor4f(doorColor.r, doorColor.g, doorColor.b, doorColor.a);
		glBegin(GL_TRIANGLE_STRIP);
		  glVertex2f(-thickness/2, -0.5);
		  glVertex2f(-thickness/2, 0.5);
		  glVertex2f(thickness/2, -0.5);
		  glVertex2f(thickness/2, 0.5);
		glEnd();
		glPopMatrix();
	}
	else if(orientation == "horizontal"){
		glPushMatrix();
		glTranslatef(global.magneticMousePoint.x, global.magneticMousePoint.y + shift, 0);
		glColor4f(doorColor.r, doorColor.g, doorColor.b, doorColor.a);
		glBegin(GL_TRIANGLE_STRIP);
		  glVertex2f(-0.5, -thickness/2);
		  glVertex2f(-0.5, thickness/2);
		  glVertex2f(0.5, -thickness/2);
		  glVertex2f(0.5, thickness/2);
		glEnd();
		glPopMatrix();
	}
}

void displayWindowPreview(Globals& global, std::vector<Module>& modules){
	std::string orientation;
	float shift;
	float thickness = modules[global.activeModuleIndex].wallThickness;
	float windowWidth = modules[global.activeModuleIndex].windowWidth;
	Color4 windowColor;

	if(global.windowPlaceable){
		windowColor = Color4(0.0, 1.0, 0.0, 0.6);
	}
	else{
		windowColor = Color4(1.0, 0.0, 0.0, 0.6);
	}

	//left wall
	if(global.magneticMousePoint.x == modules[global.activeModuleIndex].position.x){
		orientation = "vertical";
		shift = -thickness/2;
	}//right wall
	else if(global.magneticMousePoint.x == modules[global.activeModuleIndex].position.x + modules[global.activeModuleIndex].width){
		orientation = "vertical";
		shift = thickness/2;

	}//bottom wall
	else if(global.magneticMousePoint.y == modules[global.activeModuleIndex].position.y){
		orientation = "horizontal";
		shift = -thickness/2;
	}//top wall
	else if(global.magneticMousePoint.y == modules[global.activeModuleIndex].position.y + modules[global.activeModuleIndex].height){
		orientation = "horizontal";
		shift = thickness/2;
	}

	if(orientation == "vertical"){
		glPushMatrix();
		glTranslatef(global.magneticMousePoint.x + shift, global.magneticMousePoint.y, 0);
		glColor4f(windowColor.r, windowColor.g, windowColor.b, windowColor.a);
		glBegin(GL_TRIANGLE_STRIP);
		  glVertex2f(-thickness/2, -windowWidth/2);
		  glVertex2f(-thickness/2, windowWidth/2);
		  glVertex2f(thickness/2, -windowWidth/2);
		  glVertex2f(thickness/2, windowWidth/2);
		glEnd();
		glPopMatrix();
	}
	else if(orientation == "horizontal"){
		glPushMatrix();
		glTranslatef(global.magneticMousePoint.x, global.magneticMousePoint.y + shift, 0);
		glColor4f(windowColor.r, windowColor.g, windowColor.b, windowColor.a);
		glBegin(GL_TRIANGLE_STRIP);
		  glVertex2f(-windowWidth/2, -thickness/2);
		  glVertex2f(-windowWidth/2, thickness/2);
		  glVertex2f(windowWidth/2, -thickness/2);
		  glVertex2f(windowWidth/2, thickness/2);
		glEnd();
		glPopMatrix();
	}
}

void displayFurnishingPreview(Globals& global){

	Color4 rectangleFill;
	Color4 rectangleOutline;
	float width = global.furnishingPreviewWidth;
	float height = global.furnishingPreviewHeight;

	if(global.furnishingPlaceable){
		rectangleFill = Color4(0.0, 1.0, 0.0, 0.2);
		rectangleOutline = Color4(0.0, 1.0, 0.0, 0.8);
	}
	else{
		rectangleFill = Color4(1.0, 0.0, 0.0, 0.2);
		rectangleOutline = Color4(1.0, 0.0, 0.0, 0.8);
	}

	glColor4f(rectangleFill.r, rectangleFill.g, rectangleFill.b, rectangleFill.a);
	glBegin(GL_TRIANGLE_STRIP);
	  glVertex2f(global.animatedMousePoint.x - width/2, global.animatedMousePoint.y - height/2);
	  glVertex2f(global.animatedMousePoint.x - width/2, global.animatedMousePoint.y + height/2);
	  glVertex2f(global.animatedMousePoint.x + width/2, global.animatedMousePoint.y - height/2);
	  glVertex2f(global.animatedMousePoint.x + width/2, global.animatedMousePoint.y + height/2);
	glEnd();

	glColor4f(rectangleOutline.r, rectangleOutline.g, rectangleOutline.b, rectangleOutline.a);
	glBegin(GL_LINE_LOOP);
	  glVertex2f(global.animatedMousePoint.x - width/2, global.animatedMousePoint.y - height/2);
	  glVertex2f(global.animatedMousePoint.x - width/2, global.animatedMousePoint.y + height/2);
	  glVertex2f(global.animatedMousePoint.x + width/2, global.animatedMousePoint.y + height/2);
	  glVertex2f(global.animatedMousePoint.x + width/2, global.animatedMousePoint.y - height/2);
	glEnd();
}

