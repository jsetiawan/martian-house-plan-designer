/*
 * FurnishingDimensions.h
 *
 *  Created on: Mar 30, 2015
 *      Author: Jason
 */

#ifndef FURNISHINGDIMENSIONS_H_
#define FURNISHINGDIMENSIONS_H_

const float SLEEPER_WIDTH = 1;
const float SLEEPER_HEIGHT = 2;
const float SHOWER_WIDTH = 1.5;
const float SHOWER_HEIGHT = 1.5;
const float ROBE_WIDTH = 0.5;
const float ROBE_HEIGHT = 2;
const float SINK_WIDTH = 1;
const float SINK_HEIGHT = 0.5;
const float WC_WIDTH = 1;
const float WC_HEIGHT = 1;
const float TABLE_WIDTH = 1.5;
const float TABLE_HEIGHT = 1;
const float CHAIR_WIDTH = 0.5;
const float CHAIR_HEIGHT = 0.5;

#endif /* FURNISHINGDIMENSIONS_H_ */
