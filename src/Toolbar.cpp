/*
 * Toolbar.cpp
 *
 *  Created on: Mar 29, 2015
 *      Author: Jason
 */

#include "Toolbar.h"
#include <iostream>

const int PANEL_COLUMNS = 2;
const int PANEL_ROW_HEIGHT = 60;

Toolbar::Toolbar(Globals& _global, std::vector<Module>& _modules) : global(&_global), modules(&_modules){
//	std::cout << "Toolbar created" << std::endl << std::flush;
//	modules->push_back(Module(4.0, 4.0, true, Point(0.0, 0.0)));
//	std::cout << "modules->size(): " << modules->size() << std::endl << std::flush;
}

void Toolbar::mouseHandler(int button, int state, int x, int y){
	if(global->mouseInWorld(x, y)){
		if(global->worldDragMode){
			global->clickedPoint = global->mouseToWorld(x, y);
		}
		else if(button == 19){//zoom in
			if(global->worldSize > 2){
				global->worldSize -= 0.5;
			}
		}
		else if(button == 20){//zoom out
			if(global->worldSize < 17){
				global->worldSize += 0.5;
			}
		}
		else if(global->mouseInWorldBoundary(x, y)){
			if(selectedTool == "High Grade Module" || selectedTool == "Low Grade Module"){
				if(state == GLUT_UP && button == 0){
					if(global->validRectangle == true){
						float width = fabs(global->magneticMousePoint.x - global->clickedPoint.x);
						float height = fabs(global->magneticMousePoint.y - global->clickedPoint.y);
						float lowerX;
						float lowerY;
						bool highGrade;

						if(global->magneticMousePoint.x < global->clickedPoint.x){
							lowerX = global->magneticMousePoint.x;
						}
						else{
							lowerX = global->clickedPoint.x;
						}

						if(global->magneticMousePoint.y < global->clickedPoint.y){
							lowerY = global->magneticMousePoint.y;
						}
						else{
							lowerY = global->clickedPoint.y;
						}

						if(selectedTool == "High Grade Module"){
							highGrade = true;
						}
						modules->push_back(Module(width, height, highGrade, Point(lowerX, lowerY)));
						global->saveState((*modules));
						global->validRectangle = false;
					}
					global->showMagneticRectangle = false;
					global->moduleDrawingMode = false;
					glutPostRedisplay();
				}
				else if(state == GLUT_DOWN && button == 0){
					global->moduleDrawingMode = true;
					global->showMagneticCursor = false;
					global->clickedPoint = global->magneticMousePoint;
				}
			}
			else if(selectedTool == "Pressure Seal Door"){
				if(state == GLUT_UP && button == 0){
					if(global->pressureDoorPlaceable){
						(*modules)[global->activeModuleIndex].addPressureDoor(Point(global->magneticMousePoint.x, global->magneticMousePoint.y));
						global->saveState((*modules));
					}
				}
			}
			else if(selectedTool == "Window"){
				if(state == GLUT_UP && button == 0){
					if(global->windowPlaceable){
						(*modules)[global->activeModuleIndex].addWindow(Point(global->magneticMousePoint.x, global->magneticMousePoint.y));
						global->saveState((*modules));
					}
				}
			}
			else if(selectedTool == "Rotate Furnishing"){
				Point worldMouse = global->mouseToWorld(x, y);
				if(state == GLUT_DOWN && button == 0){
					for(int i = 0; i < (*modules).size(); i++){
						int tempIndex = (*modules)[i].getClickedFurnishingIndex(Point(worldMouse.x, worldMouse.y));
						if(tempIndex != -1){
							(*modules)[i].rotateFurnishing(tempIndex);
							global->saveState((*modules));
						}
					}
				}
			}
			else if(selectedTool == "Remove Furnishing"){
				Point worldMouse = global->mouseToWorld(x, y);
				if(state == GLUT_DOWN && button == 0){
					for(int i = 0; i < (*modules).size(); i++){
						int tempIndex = (*modules)[i].getClickedFurnishingIndex(Point(worldMouse.x, worldMouse.y));
						if(tempIndex != -1){
							(*modules)[i].removeFurnishing(tempIndex);
							global->saveState((*modules));
						}
					}
				}
			}
			else if(selectedTool == "Move Furnishing"){
				Point worldMouse = global->mouseToWorld(x, y);
				if(state == GLUT_DOWN && button == 0){
					global->furnishingDragMode = false;
					for(int i = 0; i < (*modules).size(); i++){
						int tempIndex = (*modules)[i].getClickedFurnishingIndex(Point(worldMouse.x, worldMouse.y));
						if(tempIndex != -1){
							global->movingFurnishing = (*modules)[i].retrieveFurnishing(tempIndex);
							global->furnishingDragMode = true;
							global->originalModuleIndexOfFurnishing = i;

							global->magneticMousePoint = global->mouseToWorld(x, y);
							global->animatedMousePoint = global->mouseToWorld(x, y);
						}
					}
				}
				if(state == GLUT_UP && button == 0 && global->furnishingDragMode){
					if(global->furnishingPlaceable){
						(*modules)[global->activeModuleIndex].restoreFurnishing(global->movingFurnishing);
						(*modules)[global->activeModuleIndex].furnishings.back().position = Point(global->magneticMousePoint.x, global->magneticMousePoint.y);
						global->saveState((*modules));
					}
					else {
						(*modules)[global->originalModuleIndexOfFurnishing].restoreFurnishing(global->movingFurnishing);
					}
					global->furnishingDragMode = false;
					global->showFurnishingPreview = false;
					global->cursorQuarterShiftX = false;
					global->cursorQuarterShiftY = false;
				}
			}
			else if(selectedTool == "Sleeper" ||
					selectedTool == "Shower" ||
					selectedTool == "Robe" ||
					selectedTool == "Sink" ||
					selectedTool == "WC" ||
					selectedTool == "Table" ||
					selectedTool == "Chair"){
				if(global->furnishingPlaceable && state == GLUT_UP && button == 0){
					(*modules)[global->activeModuleIndex].addFurnishing(selectedTool, Point(global->magneticMousePoint.x, global->magneticMousePoint.y));
					global->saveState((*modules));
				}
			}
		}
	}
	else{
		if(button == 0){
			int toolSetIndex = mouseToToolsetIndex(x, y);
			if(toolSetIndex <= toolset.size() - 1){
				selectTool(toolset[toolSetIndex].name);

				global->magneticMousePoint = global->mouseToWorld(x, y);
				global->animatedMousePoint = global->mouseToWorld(x, y);
		}
		}
	}
}

void Toolbar::motionHandler(int x, int y){
	if(global->mouseInWorld(x, y)){
		if(global->worldDragMode){
			Point draggedPoint = global->mouseToWorld(x, y);

			float displacementX = global->worldFocus.x + global->clickedPoint.x - draggedPoint.x;
			float displacementY = global->worldFocus.y + global->clickedPoint.y - draggedPoint.y;

			if(displacementX >= global->gridSize/2 || displacementX <= -global->gridSize/2){
				displacementX = global->worldFocus.x;
			}
			if(displacementY >= global->gridSize/2 || displacementY <= -global->gridSize/2){
				displacementY = global->worldFocus.y;
			}
			global->worldFocus = Point(displacementX, displacementY);
		}
		else if(global->mouseInWorldBoundary(x, y)){
			if(selectedTool == "High Grade Module" && global->moduleDrawingMode){
				if(global->showMagneticRectangle == false){
					global->showMagneticRectangle = true;
				}

				global->magnetizeMouse(x, y, false);
				float width = fabs(global->magneticMousePoint.x - global->clickedPoint.x);
				float height = fabs(global->magneticMousePoint.y - global->clickedPoint.y);

				float lowerX;
				float lowerY;

				if(global->magneticMousePoint.x < global->clickedPoint.x){
					lowerX = global->magneticMousePoint.x;
				}
				else{
					lowerX = global->clickedPoint.x;
				}

				if(global->magneticMousePoint.y < global->clickedPoint.y){
					lowerY = global->magneticMousePoint.y;
				}
				else{
					lowerY = global->clickedPoint.y;
				}

				if(width >= 2 && width <= 12 && height >= 3 && height <= 15){
					int intersectionCounter = 0;
					for(int i = 0; i < modules->size(); i++){
						if((*modules)[i].verifyNonIntersecting(Point(lowerX, lowerY), width, height, true) == false){
							intersectionCounter++;
						}
					}
					if(intersectionCounter == 0){
						global->validRectangle = true;
					}
					else{
						global->validRectangle = false;
					}
				}
				else{
					global->validRectangle = false;
				}
			}
			else if(selectedTool == "Low Grade Module" && global->moduleDrawingMode){
				if(global->showMagneticRectangle == false){
					global->showMagneticRectangle = true;
				}

				global->magnetizeMouse(x, y, true);
				float width = fabs(global->magneticMousePoint.x - global->clickedPoint.x);
				float height = fabs(global->magneticMousePoint.y - global->clickedPoint.y);

				float lowerX;
				float lowerY;

				if(global->magneticMousePoint.x < global->clickedPoint.x){
					lowerX = global->magneticMousePoint.x;
				}
				else{
					lowerX = global->clickedPoint.x;
				}

				if(global->magneticMousePoint.y < global->clickedPoint.y){
					lowerY = global->magneticMousePoint.y;
				}
				else{
					lowerY = global->clickedPoint.y;
				}

				if(width >= 2 && width <= 5 && height >= 2 && height <= 5){
					int intersectionCounter = 0;
					for(int i = 0; i < modules->size(); i++){
						if((*modules)[i].verifyNonIntersecting(Point(lowerX, lowerY), width, height, false) == false){
							intersectionCounter++;
						}
					}
					if(intersectionCounter == 0){
						global->validRectangle = true;
					}
					else{
						global->validRectangle = false;
					}
				}
				else{
					global->validRectangle = false;
				}
			}
			else if(selectedTool == "Move Furnishing" && global->furnishingDragMode){
				if(global->showFurnishingPreview == false){
					global->showFurnishingPreview = true;
				}

				std::string furnishingName = global->movingFurnishing.name;

				//enable magnetic quarter shifting for furnishings whose dimensions are in half increments
				if(furnishingName == "Shower" ||
				   furnishingName == "Robe" ||
				   furnishingName == "Table" ||
				   furnishingName == "Chair"){
					global->cursorQuarterShiftX = true;
				}

				if(furnishingName == "Shower" ||
				   furnishingName == "Sink" ||
				   furnishingName == "Chair"){
					global->cursorQuarterShiftY = true;
				}

				float width = global->movingFurnishing.width;
				float height = global->movingFurnishing.height;

				global->furnishingPreviewWidth = width;
				global->furnishingPreviewHeight = height;

				global->magnetizeMouse(x, y, true);
				global->furnishingPlaceable = false;

				for(int i = 0; i < (*modules).size(); i++){
					if((*modules)[i].verifyLegalFurnishingPlacement(width, height, Point(global->magneticMousePoint.x, global->magneticMousePoint.y), -1)){
						global->furnishingPlaceable = true;
						global->activeModuleIndex = i;
					}
				}
			}
		}
	}
}

void Toolbar::passiveMotionHandler(int x, int y){
	for(int i = 0; i < toolset.size(); i++){
		toolset[i].hover = false;
	}
	if(global->mouseInWorld(x, y)){
		if(global->mouseInWorldBoundary(x, y)){
			if(selectedTool == "High Grade Module" || selectedTool == "Low Grade Module"){
				if(global->showMagneticCursor == false){
					global->showMagneticCursor = true;
				}

				global->magnetizeMouse(x, y, true);
			}
			else if(selectedTool == "Pressure Seal Door"){
				global->pressureDoorPlaceable = false;
				global->showPressureDoorPreview = false;
				global->magnetizeMouse(x, y, true);

				for(int i = 0; i < modules->size(); i++){
					if((*modules)[i].onWall(global->magneticMousePoint.x, global->magneticMousePoint.y)){
						global->showPressureDoorPreview = true;
						global->activeModuleIndex = i;

						if((*modules)[i].verifySegmenterProximity(global->magneticMousePoint.x, global->magneticMousePoint.y, "door")){
							global->pressureDoorPlaceable = true;
						}
					}
				}
			}
			else if(selectedTool == "Window"){
				global->magnetizeMouse(x, y, true);
				global->windowPlaceable = false;
				global->showWindowPreview = false;
				global->cursorQuarterShiftY = false;
				global->cursorQuarterShiftX = false;

				for(int i = 0; i < modules->size(); i++){
					if((*modules)[i].onWall(global->magneticMousePoint.x, global->magneticMousePoint.y)){
						global->showWindowPreview = true;
						global->activeModuleIndex = i;

						if(global->magneticMousePoint.x == (*modules)[i].position.x || global->magneticMousePoint.x == (*modules)[i].position.x + (*modules)[i].width){
							global->cursorQuarterShiftY = true;
						}
						else if(global->magneticMousePoint.y == (*modules)[i].position.y || global->magneticMousePoint.y == (*modules)[i].position.y + (*modules)[i].height){
							global->cursorQuarterShiftX = true;
						}

						if((*modules)[i].verifySegmenterProximity(global->magneticMousePoint.x, global->magneticMousePoint.y, "window")){
							global->windowPlaceable = true;
						}
					}
				}
			}
			else if(selectedTool == "Sleeper" ||
					selectedTool == "Shower" ||
					selectedTool == "Robe" ||
					selectedTool == "Sink" ||
					selectedTool == "WC" ||
					selectedTool == "Table" ||
					selectedTool == "Chair"){

				if(global->showFurnishingPreview == false){
					global->showFurnishingPreview = true;
				}

				//enable magnetic quarter shifting for furnishings whose dimensions are in half increments
				if(selectedTool == "Shower" ||
				   selectedTool == "Robe" ||
				   selectedTool == "Table" ||
				   selectedTool == "Chair"){
					global->cursorQuarterShiftX = true;
				}

				if(selectedTool == "Shower" ||
				   selectedTool == "Sink" ||
				   selectedTool == "Chair"){
					global->cursorQuarterShiftY = true;
				}

				float width;
				float height;

				if(selectedTool == "Sleeper"){
					width = SLEEPER_WIDTH;
					height = SLEEPER_HEIGHT;
				}
				else if(selectedTool == "Shower"){
					width = SHOWER_WIDTH;
					height = SHOWER_HEIGHT;
				}
				else if(selectedTool == "Robe"){
					width = ROBE_WIDTH;
					height = ROBE_HEIGHT;
				}
				else if(selectedTool == "Sink"){
					width = SINK_WIDTH;
					height = SINK_HEIGHT;
				}
				else if(selectedTool == "WC"){
					width = WC_WIDTH;
					height = WC_HEIGHT;
				}
				else if(selectedTool == "Table"){
					width = TABLE_WIDTH;
					height = TABLE_HEIGHT;
				}
				else if(selectedTool == "Chair"){
					width = CHAIR_WIDTH;
					height = CHAIR_HEIGHT;
				}

				global->furnishingPreviewWidth = width;
				global->furnishingPreviewHeight = height;

				global->magnetizeMouse(x, y, true);
				global->furnishingPlaceable = false;

				for(int i = 0; i < (*modules).size(); i++){
					if((*modules)[i].verifyLegalFurnishingPlacement(width, height, Point(global->magneticMousePoint.x, global->magneticMousePoint.y), -1)){
						global->furnishingPlaceable = true;
						global->activeModuleIndex = i;
					}
				}
			}
		}
	}
	else{
		int toolSetIndex = mouseToToolsetIndex(x, y);
		if(toolSetIndex <= toolset.size() - 1){
			toolset[toolSetIndex].hover = true;
		}
	}
}

void Toolbar::keyHandler(char key, int x, int y){
	if(key == ''){// CTRL+Z
		(*modules).clear();
		(*modules) = global->retrieveState("undo");
	}

	if(key == ''){// CTRL+Y
		(*modules).clear();
		(*modules) = global->retrieveState("redo");
	}

	if(key == ''){//CTRL+S
		global->writeToFile((*modules));
	}

	if(key == ''){//CTRL+L
		global->readFromFile((*modules));
		global->saveState((*modules));
	}

	if(key == 'h'){
		selectTool("High Grade Module");
	}

	if(key == 'l'){
		selectTool("Low Grade Module");
	}

	if(key == 'p'){
		selectTool("Pressure Seal Door");
	}

	if(key == 'w'){
		selectTool("Window");
	}

	if(key == 'v'){
		selectTool("Move Furnishing");
	}

	if(key == 'r'){
		selectTool("Rotate Furnishing");
	}

	if(key == 'd'){
		selectTool("Remove Furnishing");
	}

	if(key == '1'){
		selectTool("Sleeper");
	}

	if(key == '2'){
		selectTool("Shower");
	}

	if(key == '3'){
		selectTool("Robe");
	}

	if(key == '4'){
		selectTool("Sink");
	}

	if(key == '5'){
		selectTool("WC");
	}

	if(key == '6'){
		selectTool("Table");
	}

	if(key == '7'){
		selectTool("Chair");
	}
}

void Toolbar::addTool(std::string name){
	float x = global->screenSize.x - global->panelWidth + (toolset.size() % PANEL_COLUMNS) * (global->panelWidth/(float)PANEL_COLUMNS);
	float y = global->screenSize.y - trunc((float)toolset.size()/(float)PANEL_COLUMNS) * (float)PANEL_ROW_HEIGHT;
	float width = global->panelWidth / (float)PANEL_COLUMNS;
	float height = (float)PANEL_ROW_HEIGHT;

	toolset.push_back(Tool(name, false, false, Point(x, y), Point(width, height)));
}

void Toolbar::selectTool(std::string name){
	for(int i = 0; i < toolset.size(); i++){
		toolset[i].selected = false;
		if(toolset[i].name == name){
			toolset[i].selected = true;
		}
	}

	global->showWindowPreview = false;
	global->showPressureDoorPreview = false;
	global->showFurnishingPreview = false;
	global->cursorQuarterShiftX = false;
	global->cursorQuarterShiftY = false;
	global->showMagneticCursor = false;

	selectedTool = name;
}

int Toolbar::mouseToToolsetIndex(int mouseX, int mouseY){
	float x = mouseX - global->screenSize.x + global->panelWidth;

	float column = trunc((x/global->panelWidth) * PANEL_COLUMNS);
	float row = trunc(mouseY/PANEL_ROW_HEIGHT);

	return row*PANEL_COLUMNS + column;
}

void Toolbar::display(){
	glPushMatrix();
	glTranslatef(global->screenSize.x - global->initialScreenSize.x, global->screenSize.y - global->initialScreenSize.y, 0);
	for(int i = 0; i < toolset.size(); i++){
		toolset[i].display();
	}
	glPopMatrix();
}


