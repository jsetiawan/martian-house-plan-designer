/*
 * structures.h
 *
 *  Created on: Mar 29, 2015
 *      Author: Jason
 */

#ifndef STRUCTURES_H_
#define STRUCTURES_H_

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <GL/glut.h>

struct Point{
	float x;
	float y;

	Point(float _x, float _y);

	Point();
};

struct Color4 {
	float r;
	float g;
	float b;
	float a;

public:
	Color4(float _r, float _g, float _b, float _a);

	Color4();
};

struct Wall {
	std::vector<Point> wallPath;

	Wall(std::vector<Point> _wallPath);

	Wall();
};

class WallSegmenter {
public:
	float x;
	float y;
	float displayShiftX;
	float displayShiftY;
	float width;
	float thickness;
	float wallThickness;
	std::string type;
	std::string orientation;

	explicit WallSegmenter(float _x, float _y, float _width, float _wallThickness, std::string _type, std::string _orientation);

	WallSegmenter();

	void save(std::ofstream &out);

	void load(std::ifstream &in, std::string object);

	void display();
};



#endif /* STRUCTURES_H_ */
