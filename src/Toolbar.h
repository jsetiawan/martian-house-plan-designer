/*
 * Toolbar.h
 *
 *  Created on: Mar 29, 2015
 *      Author: Jason
 */

#ifndef TOOLBAR_H_
#define TOOLBAR_H_

#include <string>
#include "Tool.h"
#include <math.h>
#include "Module.h"
#include "Globals.h"
#include "FurnishingDimensions.h"

class Toolbar {
public:
	std::vector <Tool> toolset;
	std::string selectedTool;
	Globals * global;
	std::vector<Module> * modules;

	Toolbar(Globals& global, std::vector<Module>& modules);

	void mouseHandler(int button, int state, int x, int y);

	void motionHandler(int x, int y);

	void passiveMotionHandler(int x, int y);

	void keyHandler(char key, int x, int y);

	void addTool(std::string name);

	void selectTool(std::string name);

	// Returns index of Tool that the mouse coordinates correspond to
	int mouseToToolsetIndex(int mouseX, int mouseY);

	void display();


};


#endif /* TOOLBAR_H_ */
